package net.ihe.gazelle.sb.pdqm.application;

import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.SearchParameter;
import org.hl7.fhir.r4.model.StringType;

/**
 * Pdqm authorized parameters
 */
public enum PDQmParameters {

    ID(new StringType(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID), Enumerations.SearchParamType.STRING),
    ACTIVE(new StringType(org.hl7.fhir.r4.model.Patient.SP_ACTIVE), Enumerations.SearchParamType.TOKEN),
    IDENTIFIER(new StringType(org.hl7.fhir.r4.model.Patient.SP_IDENTIFIER), Enumerations.SearchParamType.TOKEN),
    FAMILY(new StringType(org.hl7.fhir.r4.model.Patient.SP_FAMILY), Enumerations.SearchParamType.STRING),
    GIVEN(new StringType(org.hl7.fhir.r4.model.Patient.SP_ACTIVE), Enumerations.SearchParamType.STRING),
    BIRTH_DATE(new StringType(org.hl7.fhir.r4.model.Patient.SP_GIVEN), Enumerations.SearchParamType.DATE),
    ADDRESS(new StringType(org.hl7.fhir.r4.model.Patient.SP_BIRTHDATE), Enumerations.SearchParamType.STRING),
    CITY(new StringType(org.hl7.fhir.r4.model.Patient.SP_ADDRESS_CITY), Enumerations.SearchParamType.STRING),
    COUNTRY(new StringType(org.hl7.fhir.r4.model.Patient.SP_ADDRESS_COUNTRY), Enumerations.SearchParamType.STRING),
    POSTAL_CODE(new StringType(org.hl7.fhir.r4.model.Patient.SP_ADDRESS_POSTALCODE), Enumerations.SearchParamType.STRING),
    STATE(new StringType(org.hl7.fhir.r4.model.Patient.SP_ADDRESS_STATE), Enumerations.SearchParamType.STRING),
    GENDER(new StringType(org.hl7.fhir.r4.model.Patient.SP_GENDER), Enumerations.SearchParamType.TOKEN),
    TELECOM(new StringType(org.hl7.fhir.r4.model.Patient.SP_TELECOM), Enumerations.SearchParamType.TOKEN);

    private SearchParameter parameter;

    /**
     * enum constructor
     * @param name the param name
     * @param type the param type
     */
    PDQmParameters(StringType name, Enumerations.SearchParamType type) {
        this.parameter = new SearchParameter();
        parameter.setName(name.getValue());
        parameter.setType(type);
    }

    /**
     * check if param is a specific pdqm parameter
     * @param searchParameter the parameter to check
     * @return true if same as parameter
     */
    public boolean isEqualToPdqmParameter(SearchParameter searchParameter) {
        return parameter.getName().equals(searchParameter.getName()) && parameter.getType().equals(searchParameter.getType());
    }

    /**
     * check if param is a valid pdqm parameter
     * @param searchParameter the parameter to check
     * @return true if the parameter is valid
     */
    public static boolean isValidPdqmParameter(SearchParameter searchParameter) {
        boolean paramNotNullOrInvalid = false;
        if(searchParameter != null) {
            for (PDQmParameters param : PDQmParameters.values()) {
                if(!paramNotNullOrInvalid) {
                    paramNotNullOrInvalid = param.isEqualToPdqmParameter(searchParameter);
                }
            }
        }
        return paramNotNullOrInvalid;
    }
}
