package net.ihe.gazelle.sb.pdqm.application;

import net.ihe.gazelle.sb.api.business.DecodingException;
import net.ihe.gazelle.sb.api.business.StandardBlock;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import net.ihe.gazelle.sb.fhir.business.SearchParamImpl;

import java.util.List;

/**
 * Pdqm standard block
 */
public class PDQmStandardBlock implements StandardBlock<FhirSearchParameters,FhirSearchParameters, FhirResources, FhirResources> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FhirSearchParameters decode(FhirSearchParameters input) throws DecodingException {
        if (input.getSearchParameters() == null || allParamNullOrInvalid(input.getSearchParameters())) {
            throw new DecodingException();
        }
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FhirResources encode(FhirResources output) {
        return output;
    }

    /**
     * validate that there is at least one valid parameter
     * @param searchParameters the list of received parameters
     * @return true if there is at least one valid parameter
     */
    private boolean allParamNullOrInvalid(List<SearchParamImpl> searchParameters) {
        boolean allParamNullOrInvalid = true;
        for(SearchParamImpl searchParameter : searchParameters) {
            allParamNullOrInvalid = allParamNullOrInvalid && !PDQmParameters.isValidPdqmParameter(searchParameter.getSearchParameter());
        }
        return allParamNullOrInvalid;
    }

}
