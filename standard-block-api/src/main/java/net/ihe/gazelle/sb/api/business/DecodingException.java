package net.ihe.gazelle.sb.api.business;

/**
 * Decoding exception raised if a standard block failed to decode a message part.
 */
public class DecodingException extends Exception {
   private static final long serialVersionUID = -6215473891998693931L;

   /**
    * Constructs a new exception with null as its detail message. The cause is not initialized, and may subsequently be initialized by a call to
    * {@link Throwable#initCause(Throwable)}.
    */
   public DecodingException() {
   }

   /**
    * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to
    * {@link Throwable#initCause(Throwable)}.
    *
    * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    */
   public DecodingException(String message) {
      super(message);
   }

   /**
    * Constructs a new exception with the specified detail message and cause. Note that the detail/TransactionRecordingDAO message associated with
    * cause is not automatically incorporated in this exception's detail message.
    *
    * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    * @param cause   the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates that the
    *                cause is nonexistent or unknown.
    */
   public DecodingException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructs a new exception with the specified detail message, cause, suppression enabled or disabled, and writable stack trace enabled or
    * disabled.
    *
    * @param cause              the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates
    *                           that the cause is nonexistent or unknown.
    */
   public DecodingException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructs a new exception with the specified detail message, cause, suppression enabled or disabled, and writable stack trace enabled or
    * disabled.
    *
    * @param message            the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    * @param cause              the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates
    *                           that the cause is nonexistent or unknown.
    * @param enableSuppression  whether or not suppression is enabled or disabled
    * @param writableStackTrace whether or not the stack trace should be writable
    */
   public DecodingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
