package net.ihe.gazelle.sb.api.business;

/**
 * Standard block
 * @param <T> output format for decoding
 * @param <X> input format for decoding
 * @param <Y> input format for encoding
 * @param <Z> output format for encoding
 */
public interface StandardBlock<T extends DecodedContent, X extends EncodedContent, Y extends DecodedContent, Z extends EncodedContent > {

    /**
     * Decode the received data
     * @param inputs received encoded data
     * @return decoded data
     * @throws DecodingException if there is a problem during decoding
     */
    T decode(X inputs) throws DecodingException;

    /**
     * Encode the data
     * @param output received data
     * @return encoded data
     * @throws EncodingException if there is a problem during encoding
     */
    Z encode(Y output) throws EncodingException;
}
