package net.ihe.gazelle.sb.fhir.adapter.hapi;

import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.sb.fhir.adapter.HttpServletRequestTestModel;
import net.ihe.gazelle.sb.fhir.adapter.HttpServletResponseTestModel;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Package
class HapiCorrectorServiceTest {

    private HapiCorrectorService hapiCorrectorService = new HapiCorrectorService();

    /**
     * test hapi correction
     */
    @Test
    @Package
    void testCorrect() {
        Response response = hapiCorrectorService.correctHapi(new HttpServletRequestTestModel(), new HttpServletResponseTestModel(), null, null, null);
        assertNotNull(response);
    }

    /**
     * test parsing
     * @throws IOException if impossible to read message
     */
    @Test
    @Package
    void testParsing() throws IOException {
        byte[] message = "my awesome message".getBytes();
        Response response = hapiCorrectorService.parseResponse(message, new HttpServletResponseTestModel());
        assertNotNull(response);
    }


    /**
     * test parsing gzip
     * @throws IOException if impossible to read message
     */
    @Test
    @Package
    void testParsingGzip() throws IOException {
        String stringMessage = "my awesome message";
        byte[] message = stringMessage.getBytes();
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        GZIPOutputStream output = new GZIPOutputStream(byteOutput);
        output.write(message);
        output.close();
        byteOutput.close();
        HttpServletResponseTestModel httpResponse = new HttpServletResponseTestModel();
        httpResponse.addHeader("Content-Encoding", "gzip");
        Response response = hapiCorrectorService.parseResponse(byteOutput.toByteArray(), httpResponse);
        assertNotNull(response);
        assertEquals(stringMessage, response.getEntity().toString());
    }

    /**
     * test parsing xml
     * @throws IOException if impossible to read message
     */
    @Test
    @Package
    void testCorrectHapiWithContentTypeXML() throws IOException {
        byte[] message = "<my> awesome message </my>".getBytes();
        HttpServletResponseTestModel httpResponse = new HttpServletResponseTestModel();
        Response response = hapiCorrectorService.parseResponse(message, httpResponse);
        assertNotNull(response);
        assertEquals("application/fhir+xml", response.getHeaderString("Content-Type"));
    }

    /**
     * test parsing with content encoding header
     * @throws IOException if impossible to read message
     */
    @Test
    @Package
    void testCorrectHapiWithContentEncodingHeader() throws IOException {
        byte[] message = "my awesome message".getBytes();
        HttpServletResponseTestModel httpResponse = new HttpServletResponseTestModel();
        httpResponse.addHeader("Content-Encoding", "test");
        Response response = hapiCorrectorService.parseResponse(message, httpResponse);
        assertNotNull(response);
        assertNull(response.getHeaderString("Content-Encoding"));
    }

    /**
     * test parsing with test header
     * @throws IOException if impossible to read message
     */
    @Test
    @Package
    void testCorrectHapiWithTestHeader() throws IOException {
        byte[] message = "my awesome message".getBytes();
        HttpServletResponseTestModel httpResponse = new HttpServletResponseTestModel();
        httpResponse.addHeader("Test", "test");
        Response response = hapiCorrectorService.parseResponse(message, httpResponse);
        assertNotNull(response);
        assertEquals("test", response.getHeaderString("Test"));
    }

}