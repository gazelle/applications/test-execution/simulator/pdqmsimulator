package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.sb.api.business.DecodingException;
import net.ihe.gazelle.sb.api.business.EncodingException;
import net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp.MockServletConfig;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Fhir block test
 */
@Package
class FhirBlockTest {

    private FhirStandardBlock fhirStandardBlock = new FhirStandardBlock();

    /**
     * test decoding
     * @throws DecodingException if unexpected decoding error
     */
    @Test
    @Covers(requirements = {"SB-1","SB-3"})
    public void decodeTest() throws DecodingException {
        FhirSearchParameters expected = new FhirSearchParameters();
        fhirStandardBlock.setHapiCorrectorService(new HapiCorrectorServiceTestModel(expected));
        HttpContent content = new HttpContent();
        content.setConfig(new MockServletConfig());
        FhirSearchParameters response= fhirStandardBlock.decode(content);

        assertEquals(expected, response);
    }

    /**
     * test encoding
     * @throws EncodingException if unexpected encoding error
     * @throws DecodingException if unexpected decoding error
     */
    @Test
    @Covers(requirements = {"SB-2","SB-4"})
    public void encodeTest() throws EncodingException, DecodingException {
        Response expected = Response.status(Response.Status.FORBIDDEN).build();
        fhirStandardBlock.setHapiCorrectorService(new HapiCorrectorServiceTestModel(expected));
        HttpContent content = new HttpContent();
        content.setConfig(new MockServletConfig());

        fhirStandardBlock.decode(content);
        HttpResponse response= fhirStandardBlock.encode(new FhirResources());

        assertEquals(expected, response.getResponse());
    }

}