package net.ihe.gazelle.sb.fhir.adapter.hapi;

import org.junit.jupiter.api.Test;

import javax.servlet.ServletException;

import static org.junit.Assert.assertEquals;

class PatientManagerFhirServerTest {

    private PatientManagerFhirServer server = new PatientManagerFhirServer();

    private PatientManagerFhirServerTest() throws ServletException {
    }

    /**
     * Test a {@link PatientManagerFhirServer} can be initialized
     */
    @Test
    void initialize() {
        server.initialize();
        assertEquals(1,server.getResourceProviders().size());
        assertEquals(RestfulPatientResourceProvider.class,server.getResourceProviders().iterator().next().getClass());
    }
}