package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.lib.annotations.Package;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * http content test
 */
@Package
class HttpContentTest {

    /**
     * Test Http Content constructor
     */
    @Test
    public void httpContentTest() {
        HttpServletRequestTestModel request = new HttpServletRequestTestModel();
        HttpServletResponseTestModel response = new HttpServletResponseTestModel();
        ServletConfigTestModel config = new ServletConfigTestModel();

        HttpContent content = new HttpContent(config, request, response);

        assertEquals(config, content.getConfig());
        assertEquals(request, content.getRequest());
        assertEquals(response, content.getResponse());
    }

}