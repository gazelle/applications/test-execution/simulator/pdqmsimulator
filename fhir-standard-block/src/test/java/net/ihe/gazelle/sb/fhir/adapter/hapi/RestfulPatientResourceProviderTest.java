package net.ihe.gazelle.sb.fhir.adapter.hapi;

import ca.uhn.fhir.rest.param.StringParam;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import org.hl7.fhir.r4.model.IdType;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * test Hapi provider webservice
 */
class RestfulPatientResourceProviderTest {

    private CompletableFuture<FhirSearchParameters> completableFutureParams = new CompletableFuture<>();

    private CompletableFuture<FhirResources> completableFutureResourcesInput = new CompletableFuture<>();

    private CompletableFuture<FhirResources> completableFutureResourcesOutput = new CompletableFuture<>();

    /**
     * tested class
     */
    private RestfulPatientResourceProvider provider = new RestfulPatientResourceProvider(completableFutureParams, completableFutureResourcesInput);

    /**
     * test retrieve parameters
     * @throws ExecutionException if thread error
     * @throws InterruptedException if thread error
     */
    @Test
    public void retrieveTestCompleteParam() throws ExecutionException, InterruptedException {
        String id = "myId";
        FhirResources resources = new FhirResources();

        completableFutureResourcesOutput.completeAsync(() -> {resources.addResource(provider.getResourceById(new IdType(id))); return resources;}, Executors.newCachedThreadPool());

        FhirSearchParameters params = completableFutureParams.get();
        completableFutureResourcesInput.cancel(false);

        assertEquals(id, params.getSearchParameters().get(0).getValue());
    }

    /**
     * test retrieve resources
     * @throws ExecutionException if thread error
     * @throws InterruptedException if thread error
     */
    @Test
    public void retrieveTestGiveResources() throws ExecutionException, InterruptedException {
        String id = "myId";
        FhirResources input = new FhirResources();
        PatientFhirIHE patient = new PatientFhirIHE();
        input.addResource(patient);

        completableFutureResourcesOutput.completeAsync(() -> {
            FhirResources futureOutput = new FhirResources();
            futureOutput.addResource(provider.getResourceById(new IdType(id)));
            return futureOutput;
        }, Executors.newCachedThreadPool());

        FhirSearchParameters params = completableFutureParams.get();
        completableFutureResourcesInput.complete(input);

        FhirResources output = completableFutureResourcesOutput.get();

        assertEquals(1, output.getResources().size());
        assertEquals(patient, output.getResources().get(0));
    }

    /**
     * test search parameters
     * @throws ExecutionException if thread error
     * @throws InterruptedException if thread error
     */
    @Test
    public void searchTestCompleteParam() throws ExecutionException, InterruptedException {
        String id = "myId";
        FhirResources resources = new FhirResources();

        completableFutureResourcesOutput.completeAsync(() -> {
            resources.addAllResource(provider.findPatients(new StringParam(id), null, null, null, null, null, null, null, null, null, null, null, null, null));
            return resources;}, Executors.newCachedThreadPool());

        FhirSearchParameters params = completableFutureParams.get();
        completableFutureResourcesInput.cancel(false);

        assertEquals(id, params.getSearchParameters().get(0).getValue());
    }

    /**
     * test search resources
     * @throws ExecutionException if thread error
     * @throws InterruptedException if thread error
     */
    @Test
    public void searchTestGiveResources() throws ExecutionException, InterruptedException {
        String id = "myId";
        FhirResources input = new FhirResources();
        PatientFhirIHE patient = new PatientFhirIHE();
        input.addResource(patient);

        completableFutureResourcesOutput.completeAsync(() -> {
            FhirResources futureOutput = new FhirResources();
            futureOutput.addAllResource(provider.findPatients(new StringParam(id), null, null, null, null, null, null, null, null, null, null, null, null, null));
            return futureOutput;}, Executors.newCachedThreadPool());

        FhirSearchParameters params = completableFutureParams.get();
        completableFutureResourcesInput.complete(input);

        FhirResources output = completableFutureResourcesOutput.get();

        assertEquals(1, output.getResources().size());
        assertEquals(patient, output.getResources().get(0));
    }


}