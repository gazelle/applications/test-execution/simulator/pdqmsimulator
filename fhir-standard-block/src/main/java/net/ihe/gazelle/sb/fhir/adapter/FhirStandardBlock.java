package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.sb.api.business.DecodingException;
import net.ihe.gazelle.sb.api.business.EncodingException;
import net.ihe.gazelle.sb.api.business.StandardBlock;
import net.ihe.gazelle.sb.fhir.adapter.hapi.HapiCorrectorService;
import net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp.MockHttpServletRequest;
import net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp.MockHttpServletResponse;
import net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp.MockServletConfig;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;

import javax.enterprise.concurrent.ManagedExecutorService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Fhir Standard Block for fhir treatment
 */
public class FhirStandardBlock implements StandardBlock<FhirSearchParameters, HttpContent, FhirResources, HttpResponse> {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(FhirStandardBlock.class);

    private ManagedExecutorService mes;

    private Executor executor;

    private CompletableFuture<FhirSearchParameters> completableFutureParams = new CompletableFuture<>();

    private CompletableFuture<FhirResources> completableFutureResources = new CompletableFuture<>();

    private CompletableFuture<Response> completableFutureResponse = new CompletableFuture<>();

    private HapiCorrectorService hapiCorrectorService = new HapiCorrectorService();

    /**
     * Contructor
     */
    public FhirStandardBlock() {
        try {
            InitialContext ctx = new InitialContext();
            mes = (ManagedExecutorService)
                    ctx.lookup("java:comp/DefaultManagedExecutorService");
        } catch (NamingException e) {
            LOGGER.error(e, "Error instantiating ManagedExecutorService for Fhir Standard Block");
        }
        if (mes != null) {
            setExecutor(mes);
        } else {
            setExecutor(Executors.newCachedThreadPool());
        }
    }

    /**
     * Setter for the hapiCorrectorService service.
     *
     * @param hapiCorrectorService Service to use as property value
     */
    @Package
    void setHapiCorrectorService(HapiCorrectorService hapiCorrectorService) {
        this.hapiCorrectorService = hapiCorrectorService;
    }

    /**
     * Set Executor for tests
     *
     * @param executor
     */
    @Package
    void setExecutor(Executor executor) {
        this.executor = executor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FhirSearchParameters decode(HttpContent inputs) throws DecodingException {
        MockHttpServletRequest mock = new MockHttpServletRequest();
        if (inputs.getRequest() != null) {

            mock.setMethod(inputs.getRequest().getMethod());
            mock.setServerName(inputs.getRequest().getServerName());
            mock.setQueryString(inputs.getRequest().getQueryString());
            mock.setRequestURI(inputs.getRequest().getRequestURI());
        }

            MockServletConfig config = new MockServletConfig(inputs.getConfig().getServletContext(), inputs.getConfig().getServletName());

            completableFutureResponse.completeAsync(() -> hapiCorrectorService.correctHapi(mock, new MockHttpServletResponse(),
                    config, completableFutureParams, completableFutureResources), executor);
            FhirSearchParameters searchParameterList;
            try {
                searchParameterList = completableFutureParams.get();
            } catch (InterruptedException | ExecutionException e) {
                Thread.currentThread().interrupt();
                throw new DecodingException();
            }

        return searchParameterList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpResponse encode(FhirResources output) throws EncodingException {
        completableFutureResources.completeAsync(() -> output, executor);
        HttpResponse response = new HttpResponse();
        try {
            response.setResponse(completableFutureResponse.get());
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
            throw new EncodingException();
        }
        return response;
    }

}
