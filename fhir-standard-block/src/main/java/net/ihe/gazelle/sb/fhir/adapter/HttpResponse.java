package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.sb.api.business.DecodedContent;
import net.ihe.gazelle.sb.api.business.EncodedContent;

import javax.ws.rs.core.Response;

/**
 * HttpResponse, response for webservice
 */
public class HttpResponse implements EncodedContent, DecodedContent {

    private Response response;

    /**
     * get the response
     * @return a http response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * set the response
     * @param response the http response
     */
    public void setResponse(Response response) {
        this.response = response;
    }
}
