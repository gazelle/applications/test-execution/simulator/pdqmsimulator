package net.ihe.gazelle.sb.fhir.business;

import org.hl7.fhir.r4.model.SearchParameter;

public class SearchParamImpl {
    private SearchParameter searchParameter;

    private String value;

    /**
     * get the search parameter
     * @return the search parameter
     */
    public SearchParameter getSearchParameter() {
        return searchParameter;
    }

    /**
     * set the search parameter
     * @param searchParameter the search parameter to set
     */
    public void setSearchParameter(SearchParameter searchParameter) {
        this.searchParameter = searchParameter;
    }

    /**
     * get the search parameter value
     * @return the search parameter value
     */
    public String getValue() {
        return value;
    }

    /**
     * set the search parameter value
     * @param value the search parameter value to set
     */
    public void setValue(String value) {
        this.value = value;
    }



}
