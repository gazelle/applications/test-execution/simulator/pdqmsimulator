package net.ihe.gazelle.sb.fhir.business;

import net.ihe.gazelle.sb.api.business.DecodedContent;
import net.ihe.gazelle.sb.api.business.EncodedContent;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.SearchParameter;

import java.util.ArrayList;
import java.util.List;

/**
 * Fhir Search parameters
 */
public class FhirSearchParameters implements DecodedContent, EncodedContent {

    private List<SearchParamImpl> searchParameters = new ArrayList<>();

    /**
     * add new search parameter
     * @param name param name
     * @param type param type
     * @param modifier modifier
     * @param value value
     */
    public void addSearchParameter(String name, Enumerations.SearchParamType type, SearchParameter.SearchModifierCode modifier, String value) {
        SearchParameter parameter = new SearchParameter();
        parameter.setName(name);
        parameter.setType(type);
        parameter.addModifier(modifier);
        SearchParamImpl parameterImpl = new SearchParamImpl();
        parameterImpl.setSearchParameter(parameter);
        parameterImpl.setValue(value);
        searchParameters.add(parameterImpl);
    }

    /**
     * search parameter list accessor
     * @return a copy of the parameter list
     */
    public List<SearchParamImpl> getSearchParameters() {
        return new ArrayList<>(searchParameters);
    }
}

