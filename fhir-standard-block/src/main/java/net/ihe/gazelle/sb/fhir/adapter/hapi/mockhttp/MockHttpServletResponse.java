package net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class MockHttpServletResponse implements HttpServletResponse {

    private int status = 200;

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCookie(Cookie cookie) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsHeader(String name) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String encodeURL(String url) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String encodeRedirectURL(String url) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String encodeUrl(String url) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String encodeRedirectUrl(String url) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendError(int sc, String msg) throws IOException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendError(int sc) throws IOException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendRedirect(String location) throws IOException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDateHeader(String name, long date) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDateHeader(String name, long date) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHeader(String name, String value) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addHeader(String name, String value) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setIntHeader(String name, int value) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addIntHeader(String name, int value) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStatus(int sc) {
        this.status = sc;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStatus(int sc, String sm) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getStatus() {
        return status;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getHeader(String name) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getHeaders(String name) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getHeaderNames() {
        return new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCharacterEncoding() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrintWriter getWriter() throws IOException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCharacterEncoding(String charset) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setContentLength(int len) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setContentLengthLong(long len) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setContentType(String type) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBufferSize(int size) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getBufferSize() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flushBuffer() throws IOException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void resetBuffer() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCommitted() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLocale(Locale loc) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Locale getLocale() {
        return null;
    }
}
