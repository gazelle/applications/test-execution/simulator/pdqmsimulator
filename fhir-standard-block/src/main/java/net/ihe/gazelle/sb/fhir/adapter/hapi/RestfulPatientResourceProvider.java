package net.ihe.gazelle.sb.fhir.adapter.hapi;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.SearchParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * All resource providers must implement IResourceProvider
 *
 * @author aberge
 * @version $Id: $Id
 */
public class RestfulPatientResourceProvider implements IResourceProvider {

    private CompletableFuture<FhirSearchParameters> completableFutureParams;

    private CompletableFuture<FhirResources> completableFutureResources;

    /**
     * constructor for async treatment
     * @param completableFutureParams the param to send
     * @param completableFutureResources the resources to receive
     */
    public RestfulPatientResourceProvider(CompletableFuture<FhirSearchParameters> completableFutureParams, CompletableFuture<FhirResources> completableFutureResources) {
        this.completableFutureParams = completableFutureParams;
        this.completableFutureResources = completableFutureResources;
    }



    /**
     * {@inheritDoc}
     * The getResourceType method comes from IResourceProvider, and must
     * be overridden to indicate what type of resource this provider
     * supplies.
     */
    @Override
    public Class<PatientFhirIHE> getResourceType() {
        return PatientFhirIHE.class;
    }

    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param id The read operation takes one parameter, which must be of type
     *              IdDt and must be annotated with the "@Read.IdParam" annotation.
     *
     * @return Returns a resource matching this identifier, or null if none exists.
     */
    @Read()
    public PatientFhirIHE getResourceById(@IdParam IdType id) {
        FhirSearchParameters searchParameterList = new FhirSearchParameters();
        if (id.isIdPartValid()) {
                searchParameterList.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING, SearchParameter.SearchModifierCode.EXACT, id.getIdPart());
        }
        completableFutureParams.complete(searchParameterList);
        FhirResources resources = new FhirResources();
        try {
            resources = completableFutureResources.get();
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
        }
        if (resources.getResources().get(0) instanceof PatientFhirIHE) {
            return (PatientFhirIHE)resources.getResources().get(0);
        }

       else throw new InternalErrorException("error",(OperationOutcome)resources.getResources().get(0));
    }


    // PDQm ITI-78 SEARCH

    /**
     * <p>findPatients.</p>
     *
     * @param id             a {@link StringParam} object.
     * @param identifierList a {@link TokenAndListParam} object.
     * @param family         a {@link StringParam} object.
     * @param active         a {@link StringParam} object.
     * @param mothersMaidenFamilyName  a {@link StringParam} object.
     * @param given          a {@link StringParam} object.
     * @param birthdate      a {@link StringParam} object.
     * @param address        a {@link StringParam} object.
     * @param city           a {@link StringParam} object.
     * @param country        a {@link StringParam} object.
     * @param postalCode     a {@link StringParam} object.
     * @param state          a {@link StringParam} object.
     * @param gender         a {@link StringParam} object.
     * @param telecom        a {@link StringParam} object.
     *
     * @return a {@link org.hl7.fhir.r4.model.Bundle} object.
     */
    @Search(type = PatientFhirIHE.class)
    public List<PatientFhirIHE> findPatients(@OptionalParam(name = org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID) final StringParam id,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ACTIVE) final TokenParam active,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_IDENTIFIER) final TokenAndListParam identifierList,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_FAMILY) final StringAndListParam family,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_GIVEN) final StringAndListParam given,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_BIRTHDATE) final DateParam birthdate,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ADDRESS) final StringParam address,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ADDRESS_CITY) final StringParam city,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ADDRESS_COUNTRY) final StringParam country,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ADDRESS_POSTALCODE) final StringParam postalCode,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_ADDRESS_STATE) final StringParam state,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_GENDER) final TokenParam gender,
                                             @OptionalParam(name = org.hl7.fhir.r4.model.Patient.SP_TELECOM) final TokenParam telecom,
                                             @OptionalParam(name = PatientFhirIHE.SP_MOTHERS_MAIDEN_NAME_FAMILY) final StringParam
                                                     mothersMaidenFamilyName)  {

        List<PatientFhirIHE> fhirPatientList = new ArrayList<>();
        FhirSearchParameters searchParameterList = new FhirSearchParameters();
        if(id != null) {
            searchParameterList.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING, SearchParameter.SearchModifierCode.EXACT, id.getValue());
        }

        completableFutureParams.complete(searchParameterList);
        FhirResources resources = new FhirResources();
        try {
            resources = completableFutureResources.get();
        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
        }

        resources.getResources().forEach(resource -> {
            if(resource instanceof PatientFhirIHE) {
                fhirPatientList.add((PatientFhirIHE)resource);
            }
        });

    return fhirPatientList;
    }
}
