package net.ihe.gazelle.app.pdqmconnectorservice.feature.translatefhirtobusiness;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Launcher for Translate between FHIR and business model Tests
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature/translatefhirtobusiness" , glue ={"net.ihe.gazelle.app.pdqmconnectorservice.feature.translatefhirtobusiness"})
public class TranslateFhirToBusinessTest {

}
