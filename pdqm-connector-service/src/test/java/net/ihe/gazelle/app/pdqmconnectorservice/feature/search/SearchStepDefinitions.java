package net.ihe.gazelle.app.pdqmconnectorservice.feature.search;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.Chain;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.PatientSimulatorWebService;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaName;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaVerb;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.CriteriaBuilder;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.HapiFhirResourceParser;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.ResponseBundle;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.patient.PatientServiceManager;
import org.junit.Assert;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;

import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Step definitions for Search tests.
 */
public class SearchStepDefinitions {

   private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

   private PatientServiceManager patientServiceManager;

   private String stringCriteria = "";

   private Response response;

   /**
    * Set up the in-memory Database as well as services that will be tested.
    */
   @Before
   public void setUpInMemoryDBAndFeedClient() {
      this.patientServiceManager = new PatientServiceManager();
   }

   /**
    * Clean and close in-memory Database
    */
   @After
   public void tearDownInMemoryDBAndMock() {
      patientServiceManager.tearDown();
   }

   /**
    * Start the transaction before the step.
    */
   @BeforeStep
   public void startTransaction() {
      patientServiceManager.startTransaction();
   }

   /**
    * Close the transaction after the step.
    */
   @AfterStep
   public void endTransaction() {
      patientServiceManager.endTransaction();
   }

   /**
    * Initialize a cache of existing patients to be used in tests.
    *
    * @param patientDataList list of data to initialize patients from
    *
    * @throws ParseException if dates cannot be parsed.
    */
   @Given("the following patients exist")
   public void the_following_patients_exist(List<Map<String, String>> patientDataList) throws ParseException {
      patientServiceManager.addExistingPatients(patientDataList);
   }

   /**
    * Feed an existing patient from the cache to the test Patient Registry.
    *
    * @param uuid provisional UUID of the patient to feed
    *
    * @throws PatientFeedException if the patient cannot be fed.
    */
   @Given("patient is fed with provisional uuid {string}")
   public void patient_is_fed_with_uuid(String uuid) throws PatientFeedException {
      patientServiceManager.feedPatient(uuid);
   }

   /**
    * Create a search criterion for the test Search Request
    *
    * @param searchCriteriaNameString literal value of the criterion name
    * @param searchCriteriaVerbString literal value of the criterion verb
    * @param searchCriteriaValue      literal value of the criterion value
    */
   @Given("search criteria {string} {string} {string}")
   public void searchCriteriaDefinitionStep(String searchCriteriaNameString, String searchCriteriaVerbString, String searchCriteriaValue) {
      SearchCriteriaName name = SearchCriteriaName.fromString(searchCriteriaNameString);
      SearchCriteriaVerb verb = SearchCriteriaVerb.fromString(searchCriteriaVerbString);
      if (name == null && verb == null && searchCriteriaValue == null) {
         return;
      } else {
         CriteriaBuilder.checkCriterion(name, verb, searchCriteriaValue);
      }
      if (SearchCriteriaName.TMP_UUID.equals(name) && patientServiceManager.patientWasFedWithTmpUUID(searchCriteriaValue)) {
         searchCriteriaValue = patientServiceManager.getAssignedUUID(searchCriteriaValue);
      } else if (!SearchCriteriaName.UUID.equals(name)) {
         fail(String.format("Unsupported Criterion : %s", name));
      }

      String requestParameter = CriteriaBuilder.buildCriterion(name, verb, searchCriteriaValue);
      assertNotNull(requestParameter);
      if (stringCriteria.isEmpty()) {
         stringCriteria = "?";
      } else {
         stringCriteria = stringCriteria + "&";
      }
      stringCriteria = stringCriteria + requestParameter;
   }

   /**
    * Perform the search request
    */
   @When("search is done")
   public void search_is_done() {
      MockHttpServletRequest servletRequest = new MockHttpServletRequest(null);
      servletRequest.setMethod("GET");
      servletRequest.setServerName(patientServiceManager.getBaseUrl());
      servletRequest.setRequestURI("/Patient");
      servletRequest.setQueryString(stringCriteria);

      MockHttpServletResponse servletResponse = new MockHttpServletResponse();
      PatientSimulatorWebService webService = new PatientSimulatorWebService(new Chain(patientServiceManager.getPatientProcessingService()));
      webService.setConfig(new MockServletConfig());

      this.response = webService.getPatientList(servletRequest, servletResponse);
   }

   /**
    * Checks Response status.
    *
    * @param responseStatus Excpected status of the response
    */
   @Then("response is {string}")
   public void response_is(String responseStatus) {
      if ("OK".equals(responseStatus)) {
         assertNotNull("Response shall not be null !", response);
         Assert.assertEquals(200, this.response.getStatus());
      } else {
         fail(String.format("Unexpected response status to check : %s", responseStatus));
      }
   }

   /**
    * Check the number of received patients.
    *
    * @param expectedNumberOfPatients expected number of received patients.
    */
   @Then("received {int} patients")
   public void checkNumberOfReceivedPatients(int expectedNumberOfPatients) {
      ResponseBundle responseBundle = new ResponseBundle(new HapiFhirResourceParser()
            .parseBundle(this.response.getEntity().toString(), HapiFhirResourceParser.FhirResourceFormat.XML));

      assertEquals(expectedNumberOfPatients, responseBundle.getNumberOfPatientsInBundle());
   }

   /**
    * Checks that all received patient verify a criterion
    *
    * @param searchCriteriaNameString literal value of the criterion name
    * @param searchCriteriaVerbString literal value of the criterion verb
    * @param searchCriteriaValue      literal value of teh criterion value
    */
   @Then("all received patients {string} {string} {string}")
   public void allReceivedPatientsMatchCriteria(String searchCriteriaNameString, String searchCriteriaVerbString, String searchCriteriaValue) {
      SearchCriteriaName name = SearchCriteriaName.fromString(searchCriteriaNameString);
      SearchCriteriaVerb verb = SearchCriteriaVerb.fromString(searchCriteriaVerbString);
      if (name == null && verb == null && searchCriteriaValue == null) {
         return;
      } else {
         CriteriaBuilder.checkCriterion(name, verb, searchCriteriaValue);
      }

      if (SearchCriteriaName.TMP_UUID.equals(name) && patientServiceManager.patientWasFedWithTmpUUID(searchCriteriaValue)) {
         searchCriteriaValue = patientServiceManager.getAssignedUUID(searchCriteriaValue);
      } else if (!SearchCriteriaName.UUID.equals(name)) {
         fail(String.format("Unsupported Criterion : %s", name));
      }

      ResponseBundle responseBundle = new ResponseBundle(new HapiFhirResourceParser()
            .parseBundle(this.response.getEntity().toString(), HapiFhirResourceParser.FhirResourceFormat.XML));
      responseBundle.checkAttributeValueForPatients(name, verb, searchCriteriaValue);
   }
}
