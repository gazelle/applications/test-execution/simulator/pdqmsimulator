package net.ihe.gazelle.app.pdqmconnectorservice.application.recording;

import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;

import java.util.ArrayList;
import java.util.List;


public class TransactionRecordingDAOTestMock implements TransactionRecordingDAO {

   List<TransactionInstance> transactionInstances = new ArrayList<>();
   List<String> domains = new ArrayList<>();
   List<String> transactions = new ArrayList<>();
   List<String> actors = new ArrayList<>();

   /**
    * {@inheritDoc}
    */
   @Override
   public void saveTransaction(TransactionInstance transactionInstance) {
      transactionInstances.add(transactionInstance);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isDomainExisting(String domainKeyword) {
      return domains.contains(domainKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isTransactionExisting(String transactionKeyword) {
      return transactions.contains(transactionKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isActorExisting(String actorKeyword) {
      return actors.contains(actorKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createDomain(String domainKeyword) {
      domains.add(domainKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createTransaction(String transactionKeyword) {
      transactions.add(transactionKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createActor(String actorKeyword) {
      actors.add(actorKeyword);
   }

   public boolean isTransactionInstanceExisting(TransactionInstance transactionInstance) {
      return transactionInstances.contains(transactionInstance);
   }
}
