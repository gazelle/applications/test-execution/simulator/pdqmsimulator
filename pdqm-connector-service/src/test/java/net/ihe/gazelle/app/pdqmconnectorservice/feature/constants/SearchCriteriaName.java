package net.ihe.gazelle.app.pdqmconnectorservice.feature.constants;

import org.junit.Assert;

/**
 * Search Criteria names to be used in .feature tests.
 *
 * @author wbars
 */
public enum SearchCriteriaName {
    UUID("uuid", "_id"),
    TMP_UUID("tmpuuid", "_id");

    private String nameInStep;
    private String nameInRequest;

    /**
     * Default constructor for the class.
     * @param nameInStep        name in .feature step.
     * @param nameInRequest     name in FHIR request.
     */
    SearchCriteriaName(String nameInStep, String nameInRequest) {
        this.nameInStep = nameInStep;
        this.nameInRequest = nameInRequest;
    }

    /**
     * Getter for the nameInRequest property.
     * @return value of the property.
     */
    public String getNameInRequest() {
        return nameInRequest;
    }

    /**
     * Get the element from the enumeration based on its nameInStep property.
     * @param text      text to match the nameInStep property value.
     * @return corresponding {@link SearchCriteriaName}
     */
    public static SearchCriteriaName fromString(String text) {
        for (SearchCriteriaName name : SearchCriteriaName.values()) {
            if (name.nameInStep.equalsIgnoreCase(text)) {
                return name;
            }
        }
        Assert.fail("Unknown criteria name : " + text);
        return null;
    }
}