package net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir;

import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaName;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaVerb;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Resource;

import static org.junit.Assert.fail;

/**
 * Wrapper class for FHIR Bundle responses from PDQm Connector. Allows to perform checks on criterion on the wrapped response.
 */
public class ResponseBundle {

    private Bundle bundle;

    /**
     * Default constructor for the class.
     *
     * @param bundle {@link Bundle} to wrap.
     */
    public ResponseBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public int getNumberOfPatientsInBundle() {
        if (this.bundle != null) {
            return this.bundle.getTotal();
        }
        fail("Bundle shall not be null.");
        return 0;
    }

    /**
     * Checks value of attributes for all patients in wrapped bundle, based on a single criterion.
     *
     * @param name  name of the criterion
     * @param verb  verb of the criterion
     * @param value value of the criterion
     */
    public void checkAttributeValueForPatients(SearchCriteriaName name,
                                               SearchCriteriaVerb verb,
                                               String value) {
        switch (name) {
            case TMP_UUID:
            case UUID:
                checkUuidValue(verb, value);
                break;
            default:
                fail("Unexpected criteria Name : " + name);
        }
    }

    /**
     * Get a patient at a specific index in the bundle.
     *
     * @param index index of the patient;
     * @return the patient at requested index or null if the resource is not a patient.
     */
    public Patient getPatientFromIndexInBundle(int index) {
        Resource resource = bundle.getEntry().get(index).getResource();
        if (resource instanceof Patient) {
            return (Patient) resource;
        } else {
            fail("Resource in bundle shall be a Patient.");
        }
        return null;
    }

    /**
     * Check uuid value for all patients in the bundle.
     *
     * @param verb  verb to use
     * @param value expected value of the UUID
     */
    private void checkUuidValue(SearchCriteriaVerb verb, String value) {
        for (int i = 0; i < getNumberOfPatientsInBundle(); i++) {
            Patient patient = getPatientFromIndexInBundle(i);
            if (!checkStringValue(verb, value, patient.getIdElement().getIdPart())) {
                fail("One returned patient does not match the criteria : " + value + " with value : " + patient.getIdElement().getIdPart());
            }
        }
    }

    /**
     * Check a string value match depending of the given verb
     *
     * @param verb          verb to use
     * @param expectedValue expected value
     * @param value         value to check
     * @return
     */
    private boolean checkStringValue(SearchCriteriaVerb verb, String expectedValue,
                                     String value) {
        switch (verb) {
            case EQUALS:
                return expectedValue.equals(value);
            default:
                fail("No verb provided");
                return false;
        }
    }
}
