package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageInstanceMetadataTest {

    /**
     * test default constructor
     */
    @Test
    public void MessageInstanceMetadataBusinessDefaultConstructorTest() {
        MessageInstanceMetadata messageInstanceMetadata = new MessageInstanceMetadata();
        assertNotNull(messageInstanceMetadata);
    }


    @Test
    public void MessageInstanceMetadataBusinessCompleteConstructorTest() {
        MessageInstance messageInstance = new MessageInstance();
        messageInstance.setContent("content");
        String label = "label";
        String value = "value";

        MessageInstanceMetadata messageInstanceMetadata = new MessageInstanceMetadata(messageInstance,
                label, value);
        MessageInstanceMetadata messageInstanceMetadataNullLabel = new MessageInstanceMetadata(messageInstance,
                null, value);
        MessageInstanceMetadata messageInstanceMetadataNullValue = new MessageInstanceMetadata(messageInstance,
                label, null);
        MessageInstanceMetadata messageInstanceMetadataNotEquals = new MessageInstanceMetadata(new MessageInstance(),
                label, value);

        assertEquals(messageInstance, messageInstanceMetadata.getMessageInstance());
        assertEquals(label, messageInstanceMetadata.getLabel());
        assertEquals(value, messageInstanceMetadata.getValue());
        assertNull(messageInstanceMetadata.getId());
        assertNotNull(messageInstanceMetadata.hashCode());
        assertEquals(messageInstanceMetadata, messageInstanceMetadata);
        assertNotEquals(messageInstanceMetadata, messageInstanceMetadataNullLabel);
        assertNotEquals(messageInstanceMetadata, messageInstanceMetadataNullValue);
        assertNotEquals(messageInstanceMetadata, messageInstanceMetadataNotEquals);
        assertNotEquals(messageInstanceMetadata, null);
    }

}