package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MessageInstanceBusinessTest {

    @Test
    public void MessageInstanceDefaultConstructorTest() {
        MessageInstance messageInstance = new MessageInstance();
        assertTrue(messageInstance.getMetadataList().isEmpty());
    }


    @Test
    public void MessageInstanceBusinessCompleteConstructorTest() {
        String issuingActorKeyword = "issuingActorKeyword";
        byte[] content = "content".getBytes(StandardCharsets.UTF_8);
        String type = "type";
        String issuer = "issuer";
        String issuerIpAddress = "issuerIpAddress";

        MessageInstance messageInstance = new MessageInstance(issuingActorKeyword, content, type, issuer, issuerIpAddress);

        assertTrue(messageInstance.getMetadataList().isEmpty());
        assertEquals(issuingActorKeyword, messageInstance.getIssuingActorKeyword());
        assertArrayEquals(content, messageInstance.getContent());
        assertEquals(type, messageInstance.getType());
        assertEquals(issuer, messageInstance.getIssuer());
        assertEquals(issuerIpAddress, messageInstance.getIssuerIpAddress());
    }

}