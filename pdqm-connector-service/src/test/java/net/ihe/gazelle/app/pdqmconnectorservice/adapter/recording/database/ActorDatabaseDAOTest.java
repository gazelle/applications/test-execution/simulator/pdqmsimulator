package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Actor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ActorDatabaseDAOTest {

   private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

   private static EntityManagerFactory factory;
   private EntityManager entityManager;
   private ActorDatabaseDAO actorDatabaseDAO;

   @BeforeAll
   public static void setupDatabase() {
      factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   }

   @AfterAll
   public static void closeAndDropDatabase() {
      // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
      factory.close();
   }

   @BeforeEach
   public void initializeEntityManager() {
      entityManager = factory.createEntityManager();
      entityManager.getTransaction().begin();
      actorDatabaseDAO = new ActorDatabaseDAO(entityManager);
   }

   @AfterEach
   public void closeEntityManager() {
      entityManager.getTransaction().commit();
      entityManager.close();
   }


   @Test
   public void testActorCreationAndGet() {
      String keyWord = "CreationKeyword";
      actorDatabaseDAO.createActor(keyWord);

      Actor createdActor = actorDatabaseDAO.getActorByKeyword(keyWord);
      assertNotNull(createdActor);
      assertEquals(keyWord, createdActor.getKeyword());
   }

   @Test
   public void testActorCreationAndIsExisting() {
      String keyWord = "CreationAndExistKeyword";
      actorDatabaseDAO.createActor(keyWord);

      Actor createdActor = actorDatabaseDAO.getActorByKeyword(keyWord);
      assertTrue(actorDatabaseDAO.isActorExisting(keyWord));
   }

   @Test
   public void testActorNumber() {
      Long actorNumberPrev = actorDatabaseDAO.getNumberOfRegisteredActors();
      String keyWord = "NumberCreationKeyword";
      actorDatabaseDAO.createActor(keyWord);

      Long actorNumberNext = actorDatabaseDAO.getNumberOfRegisteredActors();
      assertEquals(1, actorNumberNext.compareTo(actorNumberPrev));
   }

}