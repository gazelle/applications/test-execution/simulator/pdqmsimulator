package net.ihe.gazelle.app.pdqmconnectorservice.feature.gitb;

import com.gitb.ps.ProcessRequest;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaName;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaVerb;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Wrapper for ProcessRequest sent to PR by the PDQm Connector. Allows to perform checks on criterion sent.
 */
public class ProcessRequestWrapper {

    private ProcessRequest processRequest;

    /**
     * Default constructor for the class.
     * @param processRequest    {@link ProcessRequest} to wrap
     */
    public ProcessRequestWrapper(ProcessRequest processRequest){
        this.processRequest = processRequest;
    }

    /**
     * Checks the .feature criterio has well been transformed to Gazelle Search Criterion.
     * @param name      name of the criterion
     * @param verb      verb of the criterion
     * @param value     value of the criterion
     * @throws MappingException if the {@link ProcessRequest} cannot be mapped.
     */
    public void checkAttributeValueForRequest(SearchCriteriaName name,
                                                      SearchCriteriaVerb verb, String value) throws MappingException{
        switch (name){
            case TMP_UUID:
            case UUID:
                checkUuidValue(verb, value);
                break;
            default:
                fail("Unexpected criteria Name : " + name);
        }
    }

    /**
     * Check the value for a UUID Search Criterion
     * @param verb          verb to use
     * @param uuidValue     expected criterion value
     * @throws MappingException if the {@link ProcessRequest} cannot be mapped.
     */
    private void checkUuidValue(SearchCriteriaVerb verb, String uuidValue) throws MappingException {
        SearchCriteria searchCriteria = new MapperAnyContentToObject().getObject(processRequest.getInput().get(0),SearchCriteria.class);
        assertNotNull(searchCriteria);

        StringSearchCriterion uuidSearchCriterion = (StringSearchCriterion) searchCriteria.getSearchCriterions().get(0);

        assertEquals(PatientSearchCriterionKey.UUID, uuidSearchCriterion.getKey(),
                String.format("Expected criterion with key : %s",PatientSearchCriterionKey.UUID));
        assertEquals(uuidValue, uuidSearchCriterion.getValue(),
                String.format("Value is [%s], expected [%s]", uuidSearchCriterion.getValue(), uuidValue));

        assertEquals(getSearchCriterionOperator(verb), uuidSearchCriterion.getOperator(),
                String.format("Verb is [%s], expected [%s]", uuidSearchCriterion.getOperator(), getSearchCriterionOperator(verb)));
    }

    /**
     * Get {@link StringSearchCriterionOperator} corresponding to .feature verb
     * @param verb  verb to match
     * @return value of the corresponding {@link StringSearchCriterionOperator}
     */
    private StringSearchCriterionOperator getSearchCriterionOperator(SearchCriteriaVerb verb){
        switch (verb){
            case EQUALS:
                return StringSearchCriterionOperator.EXACT;
            default:
                fail(String.format("Unexpected verb %s", verb));
                return null;
        }
    }
}
