package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import net.ihe.gazelle.app.patientregistryapi.business.Address;
import net.ihe.gazelle.app.patientregistryapi.business.AddressUse;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPoint;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPointType;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPointUse;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.StringType;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BusinessToFhirConverterTest {

   Patient patient;

   @BeforeEach
   public void init() {
      patient = new Patient();
   }

   @Test
   public void patientUUID() throws FhirConvertionException {
      patient.setUuid("uuid");
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertEquals(patient.getUuid(), fhirPatient.getId());
   }

   @Test
   public void patientNameFamily() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setFamily("Family");
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertEquals(patient.getNames().get(0).getFamily(), fhirNames.get(0).getFamily());
   }

   @Test
   public void patientMultipleName() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setFamily("Family");
      patient.addName(personName);

      PersonName personName2 = new PersonName();
      personName2.addGiven("Given");
      patient.addName(personName2);

      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(2, fhirNames.size());
   }

   @Test
   public void patientNameGiven() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.addGiven("Given");
      personName.addGiven("Dave");
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertEquals(2, fhirNames.get(0).getGiven().size());
      Assert.assertEquals(patient.getNames().get(0).getGivens().get(0), fhirNames.get(0).getGiven().get(0).getValue());
      Assert.assertEquals(patient.getNames().get(0).getGivens().get(1), fhirNames.get(0).getGiven().get(1).getValue());
   }

   @Test
   public void patientNamePrefix() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setPrefix("Prefix");
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertEquals(1, fhirNames.get(0).getPrefix().size());
      Assert.assertEquals(patient.getNames().get(0).getPrefix(), fhirNames.get(0).getPrefix().get(0).getValue());
   }

   @Test
   public void patientNameSuffix() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setSuffix("Suffix");
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertEquals(1, fhirNames.get(0).getSuffix().size());
      Assert.assertEquals(patient.getNames().get(0).getSuffix(), fhirNames.get(0).getSuffix().get(0).getValue());
   }

   @Test
   public void patientNameUse() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setUse("usual");
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertNotNull(fhirNames.get(0).getUse());
      Assert.assertEquals(HumanName.NameUse.USUAL, fhirNames.get(0).getUse());
   }

   @Test
   public void patientNameUnknownUse() {
      PersonName personName = new PersonName();
      personName.setUse("TarteAuxNoix");
      patient.addName(personName);
      assertThrows(FhirConvertionException.class, () -> BusinessToFhirConverter.patientToFhirResource(patient));
   }

   @Test
   public void patientNameNullUse() throws FhirConvertionException {
      PersonName personName = new PersonName();
      personName.setFamily("Test");
      personName.setUse(null);
      patient.addName(personName);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<HumanName> fhirNames = fhirPatient.getName();
      Assert.assertNotNull(fhirNames);
      Assert.assertEquals(1, fhirNames.size());
      Assert.assertNull(fhirNames.get(0).getUse());
   }

   @Test
   public void patientDateOfBirth() throws FhirConvertionException {
      patient.setDateOfBirth(new Date());
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertNotNull(fhirPatient.getBirthDate());
      Assert.assertEquals(patient.getDateOfBirth(), fhirPatient.getBirthDate());
   }

   @Test
   public void patientGenderFemale() throws FhirConvertionException {
      patient.setGender(GenderCode.FEMALE);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertNotNull(fhirPatient.getGender());
      Assert.assertEquals(Enumerations.AdministrativeGender.FEMALE, fhirPatient.getGender());
   }

   @Test
   public void patientGenderMale() throws FhirConvertionException {
      patient.setGender(GenderCode.MALE);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertNotNull(fhirPatient.getGender());
      Assert.assertEquals(Enumerations.AdministrativeGender.MALE, fhirPatient.getGender());
   }

   @Test
   public void patientGenderUndefined() throws FhirConvertionException {
      patient.setGender(GenderCode.UNDEFINED);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertNotNull(fhirPatient.getGender());
      Assert.assertEquals(Enumerations.AdministrativeGender.UNKNOWN, fhirPatient.getGender());
   }

   @Test
   public void patientGenderOther() throws FhirConvertionException {
      patient.setGender(GenderCode.OTHER);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      Assert.assertNotNull(fhirPatient.getGender());
      Assert.assertEquals(Enumerations.AdministrativeGender.OTHER, fhirPatient.getGender());
   }

   @Test
   public void patientIdentifier() throws FhirConvertionException {
      EntityIdentifier entityIdentifier = new EntityIdentifier();
      entityIdentifier.setSystemIdentifier("systemIdentifier");
      patient.addIdentifier(entityIdentifier);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<Identifier> fhirIdentifier = fhirPatient.getIdentifier();
      Assert.assertNotNull(fhirIdentifier);
      Assert.assertEquals(1, fhirIdentifier.size());
      Assert.assertNotNull(fhirIdentifier.get(0).getSystemElement());
      Assert.assertEquals("urn:oid:systemIdentifier", fhirIdentifier.get(0).getSystem());
   }

   @Test
   public void patientMultipleIdentifier() throws FhirConvertionException {
      EntityIdentifier entityIdentifier = new EntityIdentifier();
      entityIdentifier.setSystemIdentifier("systemIdentifier");
      patient.addIdentifier(entityIdentifier);

      EntityIdentifier entityIdentifier2 = new EntityIdentifier();
      entityIdentifier2.setSystemIdentifier("systemIdentifier2");
      patient.addIdentifier(entityIdentifier2);

      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<Identifier> fhirIdentifier = fhirPatient.getIdentifier();
      Assert.assertNotNull(fhirIdentifier);
      Assert.assertEquals(2, fhirIdentifier.size());
      Assert.assertNotNull(fhirIdentifier.get(0).getSystemElement());
      Assert.assertEquals("urn:oid:systemIdentifier", fhirIdentifier.get(0).getSystem());
      Assert.assertNotNull(fhirIdentifier.get(1).getSystemElement());
      Assert.assertEquals("urn:oid:systemIdentifier2", fhirIdentifier.get(1).getSystem());
   }

   @Test
   public void patientAddressesCity() throws FhirConvertionException {
      Address address = new Address();
      address.setCity("city");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getCity());
      Assert.assertEquals(patient.getAddresses().get(0).getCity(), fhirAddresses.get(0).getCity());
   }

   @Test
   public void patientAddressesCountry() throws FhirConvertionException {
      Address address = new Address();
      address.setCountryIso3("FR");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getCountry());
      Assert.assertEquals(patient.getAddresses().get(0).getCountryIso3(), fhirAddresses.get(0).getCountry());
   }

   @Test
   public void patientAddressesPostalCode() throws FhirConvertionException {
      Address address = new Address();
      address.setPostalCode("35700");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getPostalCode());
      Assert.assertEquals(patient.getAddresses().get(0).getPostalCode(), fhirAddresses.get(0).getPostalCode());
   }

   @Test
   public void patientAddressesState() throws FhirConvertionException {
      Address address = new Address();
      address.setState("France");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getState());
      Assert.assertEquals(patient.getAddresses().get(0).getState(), fhirAddresses.get(0).getState());
   }

   @Test
   public void patientAddressesUseHome() throws FhirConvertionException {
      Address address = new Address();
      address.setUse(AddressUse.HOME);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.HOME, fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesUseWork() throws FhirConvertionException {
      Address address = new Address();
      address.setUse(AddressUse.WORK);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.WORK, fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesUseTemporary() throws FhirConvertionException {
      Address address = new Address();
      address.setUse(AddressUse.TEMPORARY);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.TEMP, fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesUseOld() throws FhirConvertionException {
      Address address = new Address();
      address.setUse(AddressUse.BAD);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.OLD, fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesUseBilling() throws FhirConvertionException {
      Address address = new Address();
      address.setUse(AddressUse.BILLING);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNotNull(fhirAddresses.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.BILLING, fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesUseUnknown() {
      Address address = new Address();
      address.setUse(AddressUse.BIRTH_RESIDENCE);
      patient.addAddress(address);
      assertThrows(FhirConvertionException.class, () -> BusinessToFhirConverter.patientToFhirResource(patient));
   }

   @Test
   public void patientAddressesUseNull() throws FhirConvertionException {
      Address address = new Address();
      address.setCity("Test");
      address.setUse(null);
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      Assert.assertNull(fhirAddresses.get(0).getUse());
   }

   @Test
   public void patientAddressesLine() throws FhirConvertionException {
      Address address = new Address();
      address.addLine("Rue Helene Bouchard");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      List<StringType> lines = fhirAddresses.get(0).getLine();
      Assert.assertNotNull(lines);
      Assert.assertEquals(1, lines.size());
      Assert.assertEquals(patient.getAddresses().get(0).getLines().get(0), lines.get(0).getValue());
   }

   @Test
   public void patientAddressesMultipleLines() throws FhirConvertionException {
      Address address = new Address();
      address.addLine("Rue Helene Bouchard");
      address.addLine("Rue Philippe Nordmann");
      patient.addAddress(address);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.Address> fhirAddresses = fhirPatient.getAddress();
      Assert.assertNotNull(fhirAddresses);
      Assert.assertEquals(1, fhirAddresses.size());
      List<StringType> lines = fhirAddresses.get(0).getLine();
      Assert.assertNotNull(lines);
      Assert.assertEquals(2, lines.size());
      Assert.assertEquals(patient.getAddresses().get(0).getLines().get(0), lines.get(0).getValue());
      Assert.assertEquals(patient.getAddresses().get(0).getLines().get(1), lines.get(1).getValue());
   }

   @Test
   public void patientContactPointValues() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setValue("value");
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getValue());
      Assert.assertEquals(patient.getContactPoints().get(0).getValue(), fhirContactPoints.get(0).getValue());
   }

   @Test
   public void patientContactPointTypePager() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.BEEPER);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PAGER, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypePhone() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.PHONE);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PHONE, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeFax() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.FAX);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.FAX, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeUrl() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.URL);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.URL, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeEmail() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.EMAIL);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.EMAIL, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeSMS() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.SMS);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.SMS, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeOTHER() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.OTHER);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getSystem());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.OTHER, fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointTypeUnknown() {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setType(ContactPointType.DEAF_DEVICE);
      patient.addContactPoint(contactPoint);
      assertThrows(FhirConvertionException.class, () -> BusinessToFhirConverter.patientToFhirResource(patient));
   }

   @Test
   public void patientContactPointTypeNull() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setValue("Test");
      contactPoint.setType(null);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNull(fhirContactPoints.get(0).getSystem());
   }

   @Test
   public void patientContactPointUseMobile() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setUse(ContactPointUse.MOBILE);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.MOBILE, fhirContactPoints.get(0).getUse());
   }

   @Test
   public void patientContactPointUseHome() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setUse(ContactPointUse.HOME);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME, fhirContactPoints.get(0).getUse());
   }

   @Test
   public void patientContactPointUseWork() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setUse(ContactPointUse.WORK);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.WORK, fhirContactPoints.get(0).getUse());
   }

   @Test
   public void patientContactPointUseTemp() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setUse(ContactPointUse.TEMPORARY);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getUse());
      Assert.assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.TEMP, fhirContactPoints.get(0).getUse());
   }

   @Test
   public void patientContactPointUseUnknown() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setUse(ContactPointUse.ANSWERING_SERVICE);
      patient.addContactPoint(contactPoint);

      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);

      assertNull(fhirPatient.getTelecom().get(0).getUse(), "Unknown contact point usages must be ignored");
   }

   @Test
   public void patientContactPointUseNull() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setValue("Test");
      contactPoint.setUse(null);
      patient.addContactPoint(contactPoint);
      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(1, fhirContactPoints.size());
      Assert.assertNull(fhirContactPoints.get(0).getUse());
   }

   @Test
   public void patientMultipleContactPoint() throws FhirConvertionException {
      ContactPoint contactPoint = new ContactPoint();
      contactPoint.setValue("value");
      patient.addContactPoint(contactPoint);

      ContactPoint contactPoint2 = new ContactPoint();
      contactPoint2.setValue("value2");
      patient.addContactPoint(contactPoint2);

      PatientFhirIHE fhirPatient = BusinessToFhirConverter.patientToFhirResource(patient);
      Assert.assertNotNull(fhirPatient);
      List<org.hl7.fhir.r4.model.ContactPoint> fhirContactPoints = fhirPatient.getTelecom();
      Assert.assertNotNull(fhirContactPoints);
      Assert.assertEquals(2, fhirContactPoints.size());
      Assert.assertNotNull(fhirContactPoints.get(0).getValue());
      Assert.assertNotNull(fhirContactPoints.get(1).getValue());
      Assert.assertEquals(patient.getContactPoints().get(0).getValue(), fhirContactPoints.get(0).getValue());
      Assert.assertEquals(patient.getContactPoints().get(1).getValue(), fhirContactPoints.get(1).getValue());
   }
}
