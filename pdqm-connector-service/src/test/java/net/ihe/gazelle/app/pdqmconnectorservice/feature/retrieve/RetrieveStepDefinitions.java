package net.ihe.gazelle.app.pdqmconnectorservice.feature.retrieve;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.Chain;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.PatientSimulatorWebService;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.HapiFhirResourceParser;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.patient.PatientServiceManager;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Assert;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;

import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Step definitions for Retrieve tests.
 */
public class RetrieveStepDefinitions {

    private PatientServiceManager patientServiceManager;

    private Response response;

    /**
     * Set up the in-memory Database as well as services that will be tested.
     */
    @Before
    public void setUpInMemoryDBAndFeedClient() {
        this.patientServiceManager = new PatientServiceManager();
    }

    /**
     * Clean and close in-memory Database
     */
    @After
    public void tearDownInMemoryDBAndMock() {
        this.patientServiceManager.tearDown();
    }

    /**
     * Start the transaction before the step.
     */
    @BeforeStep
    public void startTransaction() {
        patientServiceManager.startTransaction();
    }

    /**
     * Close the transaction after the step.
     */
    @AfterStep
    public void endTransaction() {
        patientServiceManager.endTransaction();
    }

    /**
     * Initialize a cache of existing patients to be used in tests.
     *
     * @param patientDataList list of data to initialize patients from
     * @throws ParseException if dates cannot be parsed.
     */
    @Given("the following patients exist")
    public void the_following_patients_exist(List<Map<String, String>> patientDataList) throws ParseException {
        patientServiceManager.addExistingPatients(patientDataList);
    }

    /**
     * Feed an existing patient from the cache to the test Patient Registry.
     *
     * @param uuid provisional UUID of the patient to feed
     * @throws PatientFeedException if the patient cannot be fed.
     */
    @Given("patient is fed with provisional uuid {string}")
    public void patient_is_fed_with_uuid(String uuid) throws PatientFeedException {
        patientServiceManager.feedPatient(uuid);
    }

    /**
     * Perform the retrieve.
     *
     * @param uuid provisional UUID to search for.
     */
    @When("retrieve is done on uuid {string}")
    public void retrieveIsMade(String uuid) {
        if (patientServiceManager.patientWasFedWithTmpUUID(uuid)) {
            uuid = patientServiceManager.getAssignedUUID(uuid);
        }

        MockHttpServletRequest servletRequest = new MockHttpServletRequest(null);
        servletRequest.setMethod("GET");
        servletRequest.setServerName(patientServiceManager.getBaseUrl());
        servletRequest.setRequestURI("/Patient/" + uuid);

        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        PatientSimulatorWebService webService = new PatientSimulatorWebService(new Chain(patientServiceManager.getPatientProcessingService()));
        webService.setConfig(new MockServletConfig());

        this.response = webService.getPatientById(uuid, servletRequest, servletResponse);
    }

    /**
     * Checks Response status.
     *
     * @param responseStatus Excpected status of the response
     */
    @Then("response is {string}")
    public void response_is(String responseStatus) {
        if ("OK".equals(responseStatus)) {
            assertNotNull("Response shall not be null !", response);
            Assert.assertEquals(200, this.response.getStatus());
        } else {
            fail(String.format("Unexpected response status to check : %s", responseStatus));
        }
    }

    /**
     * Checks that retrieved Patient has the correct identifier.
     *
     * @param uuid
     */
    @Then("retrieved patient id is {string}")
    public void RetrievedPatientMatchCriteria(String uuid) {
        Patient responsePatient = new HapiFhirResourceParser()
                .parsePatient(this.response.getEntity().toString(), HapiFhirResourceParser.FhirResourceFormat.XML);
        assertNotNull("Received Patient shall not be null !", responsePatient);

        if (patientServiceManager.patientWasFedWithTmpUUID(uuid)) {
            uuid = patientServiceManager.getAssignedUUID(uuid);
        }

        assertEquals(uuid, responsePatient.getIdElement().getIdPart());
    }
}
