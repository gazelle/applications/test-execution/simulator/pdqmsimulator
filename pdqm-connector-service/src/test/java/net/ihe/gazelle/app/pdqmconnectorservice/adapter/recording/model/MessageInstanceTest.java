package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class MessageInstanceTest {

    @Test
    public void MessageInstanceDefaultConstructorTest() {
        MessageInstance messageInstance = new MessageInstance();
        assertNull(messageInstance.getMetadataList());
    }


    @Test
    public void MessageInstanceBusinessCompleteConstructorTest() {
        String issuingActorKeyword = "issuingActorKeyword";
        byte[] content = "content".getBytes(StandardCharsets.UTF_8);
        String type = "type";
        String issuer = "issuer";
        String issuerIpAddress = "issuerIpAddress";

        MessageInstance messageInstance = new MessageInstance(issuingActorKeyword, content, type, issuer, issuerIpAddress);

        assertTrue(messageInstance.getMetadataList().isEmpty());
        assertEquals(issuingActorKeyword, messageInstance.getIssuingActor().getKeyword());
        assertArrayEquals(content, messageInstance.getContent());
        assertEquals(type, messageInstance.getType());
        assertEquals(issuer, messageInstance.getIssuer());
        assertEquals(issuerIpAddress, messageInstance.getIssuerIpAddress());
        assertArrayEquals(content, messageInstance.getValidationDetailedResult());
        assertTrue(messageInstance.getValidationStatus().isEmpty());
        assertNull(messageInstance.getId());

    }

}