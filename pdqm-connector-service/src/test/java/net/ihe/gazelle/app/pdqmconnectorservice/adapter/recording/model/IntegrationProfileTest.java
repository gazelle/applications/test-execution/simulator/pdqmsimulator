package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link IntegrationProfile}
 */
public class IntegrationProfileTest {

    /**
     * Test for the ID getter
     */
    @Test
    public void testGetId() {
        IntegrationProfile integrationProfile = new IntegrationProfile();
        assertNull(integrationProfile.getId());
    }

    /**
     * Test for hashCode method
     */
    @Test
    public void testHashCode() {
        IntegrationProfile integrationProfile = new IntegrationProfile();
        assertNotNull(integrationProfile.hashCode());
    }

    /**
     * Test for equals method
     */
    @Test
    public void testEquals() {
        IntegrationProfile integrationProfile = new IntegrationProfile();
        assertEquals(integrationProfile, integrationProfile);
    }
}
