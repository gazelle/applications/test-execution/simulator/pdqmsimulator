package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link Domain}
 */
public class DomainTest {

    /**
     * Tests getter/setter for id property
     */
    @Test
    public void testID() {
        Integer id = 12;
        Domain domain = new Domain();
        domain.setId(id);
        assertEquals(id, domain.getId());
    }

    /**
     * Tests getter/setter for integrationsProfile property
     */
    @Test
    public void testIntegrationProfile() {
        Domain domain = new Domain();
        assertNull(domain.getIntegrationProfiles());
        List<IntegrationProfile> integrationProfiles = new ArrayList<>();
        domain.setIntegrationProfiles(integrationProfiles);
        assertEquals(integrationProfiles, domain.getIntegrationProfiles());
    }

    /**
     * Test for hashCode method
     */
    @Test
    public void testHashCode() {
        assertNotNull(new Domain().hashCode());
    }

    /**
     * Tests for equals method
     */
    @Test
    public void testEquals() {
        Domain domain = new Domain();
        assertNotEquals(domain, null);
        assertEquals(domain, new Domain());
        assertNotEquals(12, domain);

        Domain domain0 = new Domain();
        domain0.setKeyword("Test");
        assertNotEquals(domain, domain0);

        domain.setId(12);
        assertNotEquals(domain, new Domain());
        assertEquals(domain, domain);

        Domain domain1 = new Domain();
        domain1.setId(12);
        List<IntegrationProfile> integrationProfiles = new ArrayList<>();
        domain.setIntegrationProfiles(integrationProfiles);
        assertNotEquals(domain, domain1);

        domain1.setIntegrationProfiles(integrationProfiles);
        assertEquals(domain, domain1);
    }
}
