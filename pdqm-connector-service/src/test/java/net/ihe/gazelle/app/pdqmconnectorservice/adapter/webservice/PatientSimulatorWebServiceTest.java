package net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.Chain;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.SimulationGateImpl;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.sb.fhir.adapter.hapi.PatientManagerFhirServer;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test for {@link PatientSimulatorWebService}
 */
@Package
class PatientSimulatorWebServiceTest {

    private PatientSimulatorWebService patientSimulatorWebService = new PatientSimulatorWebService(new Chain(new SimulationGateImpl(null)));

    /**
     * Tests for {@link PatientSimulatorWebService#setConfig(ServletConfig)}
     * @throws ServletException if PatientManagerFhirServer cannot be instantiated
     */
    @Test
    public void setConfig() throws ServletException {
        patientSimulatorWebService.setConfig(new PatientManagerFhirServer());
        assertNotNull(patientSimulatorWebService);
    }
}