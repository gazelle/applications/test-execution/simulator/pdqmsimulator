package net.ihe.gazelle.app.pdqmconnectorservice.feature.search;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Launcher for Search Tests
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature/search" , glue ={"net.ihe.gazelle.app.pdqmconnectorservice.feature.search"})
public class SearchTest {

}
