Feature: Search Patient information

  Background:
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid0 | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
      | tmpuuid1 | FEMALE | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
    And patient is fed with provisional uuid "tmpuuid0"
    And patient is fed with provisional uuid "tmpuuid1"

  Scenario Outline: Nominal Patient search with one criterion
    Given search criteria "<parameter>" "<verb>" "<value>"
    When search is done
    Then response is "OK"
    And received <resultSize> patients
    And all received patients "<parameter>" "<verb>" "<value>"

    Examples: Simple cases
      | parameter | verb | value    | resultSize |
      | uuid      | is   | *        | 0          |
      | tmpuuid   | is   | tmpuuid0 | 1          |

