drop table public.cmn_message_instance_metadata if exists
drop table public.cmn_transaction_instance if exists
drop table public.cmn_message_instance if exists

drop table public.pam_phone_number if exists
drop table public.pat_patient_address if exists
drop table public.pam_person if exists
drop table pam_patient_patient_identifier if exists
drop table public.pam_patient_identifier if exists
drop table public.pam_hierarchic_designator if exists
drop table public.pat_patient if exists
drop table public.pix_cross_reference if exists

drop table public.tf_actor if exists
drop table public.tf_transaction if exists
drop table tf_domain_integration_profiles if exists
drop table public.tf_domain if exists
drop table public.tf_integration_profile if exists

drop sequence if exists cmn_message_instance_id_seq
drop sequence if exists cmn_message_instance_metadata_id_seq
drop sequence if exists cmn_transaction_instance_id_seq
drop sequence if exists pam_hierarchic_designator_id_seq
drop sequence if exists pam_patient_identifier_id_seq
drop sequence if exists pam_person_id_seq
drop sequence if exists pam_phone_number_id_seq
drop sequence if exists pat_patient_address_id_seq
drop sequence if exists pat_patient_id_seq
drop sequence if exists pix_cross_reference_id_seq
drop sequence if exists tf_actor_id_seq
drop sequence if exists tf_domain_id_seq
drop sequence if exists tf_integration_profile_id_seq
drop sequence if exists tf_transaction_id_seq
