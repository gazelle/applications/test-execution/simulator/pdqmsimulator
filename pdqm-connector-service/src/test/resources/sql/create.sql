create table public.cmn_message_instance (id integer not null, content binary(255), issuer varchar(255), issuer_ip_address varchar(255), type varchar(255), validation_detailed_result binary(255), validation_status varchar(255), issuing_actor integer, primary key (id))
create table public.cmn_message_instance_metadata (id integer not null, label varchar(255), value varchar(255), message_instance_id integer, primary key (id))
create table public.cmn_transaction_instance (id integer not null, company_keyword varchar(255), standard varchar(255), timestamp timestamp, is_visible boolean, domain_id integer, request_id integer, response_id integer, simulated_actor_id integer, transaction_id integer, primary key (id))
create table public.pam_hierarchic_designator (id integer not null, cat_usage boolean, is_default boolean, index integer, namespace_id varchar(255), prefix varchar(255), is_tool_assigning_authority boolean, type varchar(255), universal_id varchar(255), universal_id_type varchar(255), usage integer, primary key (id))
create table public.pam_patient_identifier (id integer not null, full_patient_id varchar(255), id_number varchar(255), identifier_type_code varchar(255), domain_id integer, primary key (id))
create table public.pam_person (id integer not null, contact_role_code varchar(255), first_name varchar(255), identifier varchar(255), last_name varchar(255), relationship_code varchar(255), second_name varchar(255), patient_id integer, primary key (id))
create table public.pam_phone_number (id integer not null, type varchar(255), value varchar(255), is_principal boolean, patient_id integer, person_id integer, primary key (id))
create table public.pat_patient (id integer not null, dtype varchar(255), alternate_first_name varchar(255), alternate_last_name varchar(255), alternate_mothers_maiden_name varchar(255), alternate_second_name varchar(255), alternate_third_name varchar(255), character_set varchar(255), country_code varchar(255), creation_date timestamp, date_of_birth timestamp, first_name varchar(255), gender_code varchar(255), last_name varchar(255), mother_maiden_name varchar(255), race_code varchar(255), religion_code varchar(255), second_name varchar(255), size integer, third_name varchar(255), weight integer, account_number varchar(255), birth_order integer, birth_place_name varchar(255), blood_group varchar(255), creator varchar(255), dmetaphone_first_name varchar(255), dmetaphone_last_name varchar(255), dmetaphone_mother_maiden_name varchar(255), email varchar(255), identity_reliability_code varchar(255), last_update_facility varchar(255), marital_status varchar(255), multiple_birth_indicator boolean, is_patient_dead boolean, patient_death_time timestamp, simulated_actor_keyword varchar(255), still_active boolean, is_test_data boolean, uuid varchar(255), vip_indicator varchar(255), cross_reference_id integer, primary key (id))
create table public.pat_patient_address (DTYPE varchar(31) not null, id integer not null, address_line varchar(255), address_type integer, city varchar(255), country_code varchar(255), state varchar(255), street_number varchar(255), zip_code varchar(255), is_main_address boolean, person_id integer, patient_id integer, primary key (id))
create table public.pix_cross_reference (id integer not null, last_changed timestamp, last_modifier varchar(255), primary key (id))
create table public.tf_actor (id integer not null, description varchar(255), keyword varchar(128) not null, name varchar(128), can_act_as_responder boolean, primary key (id))
create table public.tf_domain (id integer not null, description varchar(255), keyword varchar(128) not null, name varchar(128), primary key (id))
create table public.tf_integration_profile (id integer not null, description varchar(255), keyword varchar(128) not null, name varchar(128), primary key (id))
create table public.tf_transaction (id integer not null, description varchar(255), keyword varchar(128) not null, name varchar(128), primary key (id))
alter table public.pam_patient_identifier add constraint UK3xtcsyky3tf2htxn89q019mgw unique (full_patient_id, identifier_type_code)
alter table public.tf_actor add constraint UK_fpb6vpa9bnw6cjsb1pucuuivw unique (keyword)
alter table public.tf_domain add constraint UK_436tct1jl8811q2xgd8bjth9q unique (keyword)
alter table public.tf_integration_profile add constraint UK_ny4glgtyovt2w045nwct19arx unique (keyword)
alter table public.tf_transaction add constraint UK_6nt35dm69gbrrj0aegkkqalr2 unique (keyword)
create sequence cmn_message_instance_id_seq start with 1 increment by 1
create sequence cmn_message_instance_metadata_id_seq start with 1 increment by 1
create sequence cmn_transaction_instance_id_seq start with 1 increment by 1
create sequence pam_hierarchic_designator_id_seq start with 1 increment by 1
create sequence pam_patient_identifier_id_seq start with 1 increment by 1
create sequence pam_person_id_seq start with 1 increment by 1
create sequence pam_phone_number_id_seq start with 1 increment by 1
create sequence pat_patient_address_id_seq start with 1 increment by 1
create sequence pat_patient_id_seq start with 1 increment by 1
create sequence pix_cross_reference_id_seq start with 1 increment by 1
create sequence tf_actor_id_seq start with 1 increment by 1
create sequence tf_domain_id_seq start with 1 increment by 1
create sequence tf_integration_profile_id_seq start with 1 increment by 1
create sequence tf_transaction_id_seq start with 1 increment by 1
create table pam_patient_patient_identifier (patient_id integer not null, patient_identifier_id integer not null)
create table tf_domain_integration_profiles (domain_id integer not null, integration_profile_id integer not null)
alter table pam_patient_patient_identifier add constraint UKelwqa9mo5gr8aaff9n5rb1td2 unique (patient_id, patient_identifier_id)
alter table tf_domain_integration_profiles add constraint UKiw9qgddyj9xsshmek3gr6u588 unique (integration_profile_id, domain_id)
alter table public.cmn_message_instance add constraint FKcp2h3t2j10t763fm0hhjnxmsd foreign key (issuing_actor) references public.tf_actor
alter table public.cmn_message_instance_metadata add constraint FKpulyqs19ami3v6dro8rdalfgt foreign key (message_instance_id) references public.cmn_message_instance
alter table public.cmn_transaction_instance add constraint FKjtoukcnuiqa9rl16qru8708ky foreign key (domain_id) references public.tf_domain
alter table public.cmn_transaction_instance add constraint FK2w1k3u4dgwe2p3hdt16tuyat4 foreign key (request_id) references public.cmn_message_instance
alter table public.cmn_transaction_instance add constraint FKtpvlexyahx1ajt3yjxccbwfcy foreign key (response_id) references public.cmn_message_instance
alter table public.cmn_transaction_instance add constraint FKklw9lfgftiro9tttxe191q8bo foreign key (simulated_actor_id) references public.tf_actor
alter table public.cmn_transaction_instance add constraint FKdu8h7lrjv1aiy425u8trn5jqr foreign key (transaction_id) references public.tf_transaction
alter table public.pam_patient_identifier add constraint FK1a83v0783ptcr4axd7kq2ea7u foreign key (domain_id) references public.pam_hierarchic_designator
alter table public.pam_person add constraint FKh35y87ktgtlnp55c5u906tfek foreign key (patient_id) references public.pat_patient
alter table public.pam_phone_number add constraint FKrdfg1521cyrteyns83sx27k3u foreign key (patient_id) references public.pat_patient
alter table public.pam_phone_number add constraint FK6bh5thyxk4x3eg0nmu4q6t3o3 foreign key (person_id) references public.pam_person
alter table public.pat_patient add constraint FK1s8ood8fhlfc1so2cgr99lus6 foreign key (cross_reference_id) references public.pix_cross_reference
alter table public.pat_patient_address add constraint FKlgvcimoqfxw1yygi67h8dporg foreign key (person_id) references public.pam_person
alter table public.pat_patient_address add constraint FKeqthhmig1bhjwvl1xj826trqg foreign key (patient_id) references public.pat_patient
alter table pam_patient_patient_identifier add constraint FKadat3ednvbei5v6072tm131lw foreign key (patient_identifier_id) references public.pam_patient_identifier
alter table pam_patient_patient_identifier add constraint FKt9ew6h2vvme8t4hgroj0iinnc foreign key (patient_id) references public.pat_patient
alter table tf_domain_integration_profiles add constraint FKquvqgk1a7ynuw75wm8iqjsxou foreign key (integration_profile_id) references public.tf_integration_profile
alter table tf_domain_integration_profiles add constraint FKr1g4gewtkdcn3klm54xyvithm foreign key (domain_id) references public.tf_domain