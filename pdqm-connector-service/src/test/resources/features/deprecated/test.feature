Feature: Testing pdqm simulator get patients
  Scenario: get patients
    When user queries patients
    And transaction is intercepted
    Then return response code should be 200
    And interaction is present in database