Feature: Translate FHIR request parameters to Gazelle SearchCriteria

  Scenario: Request with exact id
    Given search criteria "uuid" "is" "123"
    When search is done
    Then forwarded parameter contains "uuid" "is" "123"


  Scenario: Patient registry returns patient with uuid
    Given the following patients exist:
      | uuid  |
      | uuid0 |
    And patients have the following identifiers:
      | uuid  | domain  | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "uuid" "is" "uuid0"
