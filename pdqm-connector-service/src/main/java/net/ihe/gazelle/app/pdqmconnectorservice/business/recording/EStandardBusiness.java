package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

/**
 * Possible standard for PDQm (incomplete)
 */
public enum EStandardBusiness {
   FHIR_XML("FHIR_XML"),
   FHIR_JSON("FHIR_JSON");

   private String value;

   /**
    * Constructor
    *
    * @param value Standard name
    */
   private EStandardBusiness(String value) {
      this.value = value;
   }

   /**
    * The name of the standard
    *
    * @return the name of the standard
    */
   @Override
   public String toString() {
      return this.value;
   }
}
