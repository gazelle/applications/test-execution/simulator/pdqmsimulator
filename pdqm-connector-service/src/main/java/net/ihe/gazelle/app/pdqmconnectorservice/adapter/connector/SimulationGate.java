package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.sb.api.business.DecodedContent;

/**
 * Interface for the Simulation Gate of a Simulator.
 * @param <T>       type of the input of the {@link SimulationGate}
 * @param <X>       type of the output of the {@link SimulationGate}
 */
public interface SimulationGate<T extends DecodedContent, X extends DecodedContent> {

    /**
     * Process the input to simulate the behavior of a real system.
     * @param content       input to the simulation
     * @return simulated response corresponding to the given input.
     * @throws SearchException if any error appears during the simulation
     */
    X process(T content) throws SearchException;
}
