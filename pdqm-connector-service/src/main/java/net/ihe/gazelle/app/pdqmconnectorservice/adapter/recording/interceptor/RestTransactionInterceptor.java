package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.interceptor;

import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionRecordingService;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Transaction Interceptor to save the transaction when a response is sent back.
 */
@Provider
public class RestTransactionInterceptor implements ContainerResponseFilter {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(RestTransactionInterceptor.class);

   @Context
   private HttpServletRequest servletRequest;

   @Inject
   private TransactionRecordingService recordingService;
   private PDQmTransactionInstanceMapper pdqmTransactionInstanceMapper = new PDQmTransactionInstanceMapper();

    /**
     * Default empty constructor for injection.
     */
    public RestTransactionInterceptor() {
        //Empty Constructor
    }

   /**
    * Intercept the response and save it as a transaction
    *
    * @param reqContext the request context
    * @param resContext the response context
    *
    * @throws IOException if error
    */
   @Override
   public void filter(ContainerRequestContext reqContext, ContainerResponseContext resContext) {
      try {
         this.recordingService.recordTransaction(
               pdqmTransactionInstanceMapper.getTransactionInstanceFromRequestResponseContext(reqContext, resContext, servletRequest));
      } catch (Exception e) {
          LOGGER.error(e, "Error recording the transaction !");
      }
   }

}