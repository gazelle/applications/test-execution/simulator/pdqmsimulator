package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * <p>Domain class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name="tf_domain", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="keyword"))
@SequenceGenerator(name="tf_domain_sequence", sequenceName="tf_domain_id_seq", allocationSize=1)
public class Domain extends TFObject implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 6652832577180218448L;

    @Id
    @GeneratedValue(generator="tf_domain_sequence", strategy=GenerationType.SEQUENCE)
    @Column(name="id", nullable=false, unique=true)
    private Integer id;

    @ManyToMany
    @JoinTable(name="tf_domain_integration_profiles",
            joinColumns=@JoinColumn(name="domain_id"),
            inverseJoinColumns=@JoinColumn(name="integration_profile_id"),
            uniqueConstraints=@UniqueConstraint(columnNames={"integration_profile_id","domain_id"}))
    private List<IntegrationProfile> integrationProfiles;

    /**
     * <p>Constructor for Domain.</p>
     */
    public Domain() {
        //Nothing to do in constructor.
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>integrationProfiles</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<IntegrationProfile> getIntegrationProfiles() {
        return integrationProfiles;
    }

    /**
     * <p>Setter for the field <code>integrationProfiles</code>.</p>
     *
     * @param integrationProfiles a {@link java.util.List} object.
     */
    public void setIntegrationProfiles(List<IntegrationProfile> integrationProfiles) {
        this.integrationProfiles = integrationProfiles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Domain domain = (Domain) o;

        if (id != null ? !id.equals(domain.id) : domain.id != null) {
            return false;
        }
        return integrationProfiles != null ? integrationProfiles.equals(domain.integrationProfiles) : domain.integrationProfiles == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (integrationProfiles != null ? integrationProfiles.hashCode() : 0);
        return result;
    }
}
