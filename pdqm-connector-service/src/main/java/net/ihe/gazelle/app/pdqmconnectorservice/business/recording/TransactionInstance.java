package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

import java.util.Date;

/**
 * Instance of transaction to record as simulation log
 */
public class TransactionInstance {

   private Date timestamp;
   private Boolean visible;
   private String domainKeyword;
   private String simulatedActorKeyword;
   private String transactionKeyword;
   private EStandardBusiness standard;
   private String companyKeyword;
   private MessageInstance request;
   private MessageInstance response;

   /**
    * Constructor for TransactionInstance.
    */
   public TransactionInstance() {
      this.visible = true;
      this.request = new MessageInstance();
      this.response = new MessageInstance();
   }

   /**
    * Constructor
    *
    * @param timestamp             Timestamp of the transaction recording
    * @param visible               Is the transaction visible to all
    * @param domainKeyword         Keyword of the domain
    * @param simulatedActorKeyword Keyword of the simulated actor
    * @param transactionKeyword    Keyword of the transaction
    * @param standard              Underlying standard involved
    * @param companyKeyword        Keyword of the SUT's organization
    * @param request               Message instance of the request
    * @param response              Message instance of the response
    */
   public TransactionInstance(Date timestamp, Boolean visible, String domainKeyword, String simulatedActorKeyword, String transactionKeyword,
                              EStandardBusiness standard, String companyKeyword, MessageInstance request, MessageInstance response) {
      this.setTimestamp(timestamp);
      this.setVisible(visible);
      this.setDomainKeyword(domainKeyword);
      this.setSimulatedActorKeyword(simulatedActorKeyword);
      this.setTransactionKeyword(transactionKeyword);
      this.setStandard(standard);
      this.setCompanyKeyword(companyKeyword);
      this.setRequest(request);
      this.setResponse(response);
   }

   /**
    * Get the recording timestamp of the transaction instance.
    *
    * @return the recording timestamp of the transaction instance
    */
   public Date getTimestamp() {
      return timestamp;
   }

   /**
    * Set the recording timestamp of the transaction instance.
    *
    * @param timestamp the recording timestamp of the transaction instance
    */
   public void setTimestamp(Date timestamp) {
      if (timestamp != null) {
         this.timestamp = (Date) timestamp.clone();
      } else {
         this.timestamp = null;
      }
   }

   /**
    * Is the transaction visible
    *
    * @return true if the transaction is visible, false otherwise
    */
   public Boolean getVisible() {
      return visible;
   }

   /**
    * Set the transaction visibility
    *
    * @param visible transaction visibility
    */
   public void setVisible(Boolean visible) {
      this.visible = visible;
   }

   /**
    * Get the keyword of the domain
    *
    * @return the keyword of the domain
    */
   public String getDomainKeyword() {
      return domainKeyword;
   }

   /**
    * Set the keyword of the domain
    *
    * @param domainKeyword the keyword of the domain
    */
   public void setDomainKeyword(String domainKeyword) {
      this.domainKeyword = domainKeyword;
   }

   /**
    * Get the keyword of the simulated actor
    *
    * @return the keyword of the simulated actor
    */
   public String getSimulatedActorKeyword() {
      return simulatedActorKeyword;
   }

   /**
    * Set the keyword of the simulated actor
    *
    * @param simulatedActorKeyword the keyword of the simulated actor
    */
   public void setSimulatedActorKeyword(String simulatedActorKeyword) {
      this.simulatedActorKeyword = simulatedActorKeyword;
   }

   /**
    * Get the keyword of the transaction
    *
    * @return the keyword of the transaction
    */
   public String getTransactionKeyword() {
      return transactionKeyword;
   }

   /**
    * Set the keyword of the transaction
    *
    * @param transactionKeyword the keyword of the transaction
    */
   public void setTransactionKeyword(String transactionKeyword) {
      this.transactionKeyword = transactionKeyword;
   }

   /**
    * Get the related underlying standard
    *
    * @return the related underlying standard
    */
   public EStandardBusiness getStandard() {
      return standard;
   }

   /**
    * Set the related underlying standard
    *
    * @param standard the related underlying standard
    */
   public void setStandard(EStandardBusiness standard) {
      this.standard = standard;
   }

   /**
    * Get the keyword of the SUT's company
    *
    * @return the keyword of the SUT's company
    */
   public String getCompanyKeyword() {
      return companyKeyword;
   }

   /**
    * Set the keyword of the SUT's company
    *
    * @param companyKeyword the keyword of the SUT's company
    */
   public void setCompanyKeyword(String companyKeyword) {
      this.companyKeyword = companyKeyword;
   }

   /**
    * Get the message instance of the request
    *
    * @return the message instance of the request
    */
   public MessageInstance getRequest() {
      return request;
   }

   /**
    * Set the message instance of the request
    *
    * @param request the message instance of the request
    */
   public void setRequest(MessageInstance request) {
      this.request = request;
   }

   /**
    * Get the message instance of the response
    *
    * @return the message instance of the response
    */
   public MessageInstance getResponse() {
      return response;
   }

   /**
    * Set the message instance of the response
    *
    * @param response the message instance of the response
    */
   public void setResponse(MessageInstance response) {
      this.response = response;
   }
}
