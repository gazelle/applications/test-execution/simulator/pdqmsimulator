package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class description</b>: TransactionInstanceDB
 *
 * This class represents an entity which is used to store an instance of a transaction (request + response)
 *
 * @author 	Anne-Gaëlle Bergé / IHE Europe

 */

@Entity
@Table(name="cmn_transaction_instance", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cmn_transaction_instance_sequence", sequenceName="cmn_transaction_instance_id_seq", allocationSize = 1)
public class TransactionInstanceDB implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = -4930989278477898216L;

    @Id
    @GeneratedValue(generator="cmn_transaction_instance_sequence", strategy=GenerationType.SEQUENCE)
    @Column(name="id", nullable=false, unique=true)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="timestamp")
    private Date timestamp;

    @Column(name="is_visible")
    private Boolean visible;

    @JoinColumn(name="domain_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Domain domain;

    @JoinColumn(name="simulated_actor_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Actor simulatedActor;

    @JoinColumn(name="transaction_id")
    @ManyToOne(fetch=FetchType.EAGER)
    private Transaction transaction;

    @Enumerated(EnumType.STRING)
    @Column(name = "standard")
    private EStandard standard;

    @Column(name="company_keyword")
    private String companyKeyword;

    @OneToOne(targetEntity= MessageInstance.class, cascade=CascadeType.ALL)
    @JoinColumn(name="request_id")
    private MessageInstance request;

    @OneToOne(targetEntity=MessageInstance.class, cascade=CascadeType.ALL)
    @JoinColumn(name="response_id")
    private MessageInstance response;

    /**
     * <p>Constructor for TransactionInstanceDB.</p>
     */
    public TransactionInstanceDB()
    {
        this.visible = true;
        this.request = new MessageInstance();
        this.response = new MessageInstance();
    }

    public TransactionInstanceDB(Date timestamp, Boolean visible, String domainKeyword, String simulatedActorKeyword, String transactionKeyword, EStandard standard, String companyKeyword, MessageInstance request, MessageInstance response) {
        this.setTimestamp(timestamp);
        this.setVisible(visible);
        Domain domain =  new Domain();
        domain.setKeyword(domainKeyword);
        this.setDomain(domain);
        Actor actor = new Actor();
        actor.setKeyword(simulatedActorKeyword);
        this.setSimulatedActor(actor);
        Transaction transaction = new Transaction();
        transaction.setKeyword(transactionKeyword);
        this.setTransaction(transaction);
        this.setStandard(standard);
        this.setCompanyKeyword(companyKeyword);
        this.setRequest(request);
        this.setResponse(response);
    }

    /**
     * <p>Getter for the field <code>standard</code>.</p>
     *
     * @return a {@link EStandard} object.
     */
    public EStandard getStandard() {
        return standard;
    }

    /**
     * <p>Setter for the field <code>standard</code>.</p>
     *
     * @param standard a {@link EStandard} object.
     */
    public void setStandard(EStandard standard) {
        this.standard = standard;
    }

    /**
     * <p>Getter for the field <code>timestamp</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * <p>Setter for the field <code>timestamp</code>.</p>
     *
     * @param timestamp a {@link java.util.Date} object.
     */
    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = (Date) timestamp.clone();
        } else {
            this.timestamp = null;
        }
    }

    /**
     * <p>Getter for the field <code>visible</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * <p>Setter for the field <code>visible</code>.</p>
     *
     * @param visible a {@link java.lang.Boolean} object.
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link Domain} object.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link Domain} object.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>transaction</code>.</p>
     *
     * @return a {@link Transaction} object.
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * <p>Setter for the field <code>transaction</code>.</p>
     *
     * @param transaction a {@link Transaction} object.
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * <p>Getter for the field <code>companyKeyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCompanyKeyword() {
        return companyKeyword;
    }

    /**
     * <p>Setter for the field <code>companyKeyword</code>.</p>
     *
     * @param companyKeyword a {@link java.lang.String} object.
     */
    public void setCompanyKeyword(String companyKeyword) {
        this.companyKeyword = companyKeyword;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>request</code>.</p>
     *
     * @return a {@link MessageInstance} object.
     */
    public MessageInstance getRequest() {
        return request;
    }

    /**
     * <p>Setter for the field <code>request</code>.</p>
     *
     * @param request a {@link MessageInstance} object.
     */
    public void setRequest(MessageInstance request) {
        this.request = request;
    }

    /**
     * <p>Getter for the field <code>response</code>.</p>
     *
     * @return a {@link MessageInstance} object.
     */
    public MessageInstance getResponse() {
        return response;
    }

    /**
     * <p>Setter for the field <code>response</code>.</p>
     *
     * @param response a {@link MessageInstance} object.
     */
    public void setResponse(MessageInstance response) {
        this.response = response;
    }

}
