package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by aberge on 03/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "cmn_message_instance_metadata", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cmn_message_instance_metadata_sequence", sequenceName = "cmn_message_instance_metadata_id_seq", allocationSize = 1)
public class MessageInstanceMetadata implements Serializable{

    public static final String MESSAGE_ID_LABEL = "Message ID";

    @Id
    @GeneratedValue(generator = "cmn_message_instance_metadata_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "label")
    private String label;

    @Column(name = "value")
    private String value;

    @ManyToOne(targetEntity = MessageInstance.class)
    @JoinColumn(name = "message_instance_id")
    private MessageInstance messageInstance;

    /**
     * <p>Constructor for MessageInstanceMetadata.</p>
     */
    public MessageInstanceMetadata(){
    }

    /**
     * <p>Constructor for MessageInstanceMetadata.</p>
     *
     * @param message a {@link MessageInstance} object.
     * @param label a {@link java.lang.String} object.
     * @param value a {@link java.lang.String} object.
     */
    public MessageInstanceMetadata(MessageInstance message, String label, String value){
        this.setMessageInstance(message);
        this.setLabel(label);
        this.setValue(value);
    }


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }


    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    /**
     * <p>Setter for the field <code>label</code>.</p>
     *
     * @param label a {@link java.lang.String} object.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>Setter for the field <code>value</code>.</p>
     *
     * @param value a {@link java.lang.String} object.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * <p>Getter for the field <code>messageInstance</code>.</p>
     *
     * @return a {@link MessageInstance} object.
     */
    public MessageInstance getMessageInstance() {
        return messageInstance;
    }

    /**
     * <p>Setter for the field <code>messageInstance</code>.</p>
     *
     * @param messageInstance a {@link MessageInstance} object.
     */
    public void setMessageInstance(MessageInstance messageInstance) {
        this.messageInstance = messageInstance;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MessageInstanceMetadata that = (MessageInstanceMetadata) o;

        if (label != null ? !label.equals(that.label) : that.label != null) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        return Objects.equals(messageInstance, that.messageInstance);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = label != null ? label.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (messageInstance != null ? messageInstance.hashCode() : 0);
        return result;
    }
}
