package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

/**
 * Interface defined for the Outside Gate of a Simulator.
 */
public interface OutsideGate {

    /**
     * Initialize the chain used by the {@link OutsideGate}.
     * @return the initialized {@link Chain}.
     */
    Chain initChain();
}
