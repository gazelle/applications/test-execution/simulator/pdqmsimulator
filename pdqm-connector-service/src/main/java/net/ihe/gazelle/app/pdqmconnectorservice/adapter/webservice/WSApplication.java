package net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice;


import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.interceptor.RestTransactionInterceptor;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Our Application
 */
@ApplicationPath("/")
public class WSApplication extends Application {

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(PatientSimulatorWebService.class);
        s.add(RestTransactionInterceptor.class);
        s.add(DefaultWebService.class);
        return s;
    }
}
