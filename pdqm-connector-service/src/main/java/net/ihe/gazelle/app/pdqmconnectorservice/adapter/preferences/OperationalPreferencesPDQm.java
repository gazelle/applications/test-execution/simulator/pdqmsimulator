package net.ihe.gazelle.app.pdqmconnectorservice.adapter.preferences;

import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Operation Preference Client to be used by the Preferences module to retrieve PDQm Connector Operational Preferences from JNDI context.
 */
public class OperationalPreferencesPDQm implements OperationalPreferencesClientApplication {

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        List<String> deploymentPreferences = new ArrayList<>() ;

        deploymentPreferences.add(Preferences.PATIENT_REGISTRY_URL.toString());

        mandatoryPreferences.put(Namespaces.DEPLOYMENT.getValue(), deploymentPreferences);
        return mandatoryPreferences;
    }
}
