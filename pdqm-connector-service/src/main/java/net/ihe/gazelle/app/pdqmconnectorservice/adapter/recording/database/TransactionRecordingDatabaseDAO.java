package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;


import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.TransactionInstanceDB;
import net.ihe.gazelle.app.pdqmconnectorservice.application.recording.TransactionRecordingDAO;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.RecordingException;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import static net.ihe.gazelle.app.pdqmconnectorservice.adapter.dependencyinjection.EntityManagerProducer.InjectEntityManager;

/**
 * Transaction Recording DAO implementation for database.
 */
public class TransactionRecordingDatabaseDAO implements TransactionRecordingDAO {

   private EntityManager entityManager;
   private DomainDatabaseDAO daoDomain;
   private ActorDatabaseDAO daoActor;
   private TransactionDatabaseDAO daoTransaction;

   private MapperAdaptor mapper = new MapperAdaptor();

   /**
    * constructor
    */
   @Inject
   public TransactionRecordingDatabaseDAO(@InjectEntityManager EntityManager entityManager) {
      this.entityManager = entityManager;
      this.daoDomain = new DomainDatabaseDAO(entityManager);
      this.daoActor = new ActorDatabaseDAO(entityManager);
      this.daoTransaction = new TransactionDatabaseDAO(entityManager);
   }


   /**
    * {@inheritDoc}
    */
   public void saveTransaction(TransactionInstance transactionInstanceBusiness) {

      TransactionInstanceDB transactionInstanceDB = mapper
            .map(transactionInstanceBusiness, TransactionInstanceDB.class);
      transactionInstanceDB.setDomain(daoDomain.getDomainByKeyword(transactionInstanceBusiness.getDomainKeyword()));
      transactionInstanceDB.setSimulatedActor(daoActor.getActorByKeyword(transactionInstanceBusiness.getSimulatedActorKeyword()));
      transactionInstanceDB.getRequest()
            .setIssuingActor(daoActor.getActorByKeyword(transactionInstanceBusiness.getRequest().getIssuingActorKeyword()));
      transactionInstanceDB.getResponse().setIssuingActor(
            daoActor.getActorByKeyword(transactionInstanceBusiness.getResponse().getIssuingActorKeyword()));
      transactionInstanceDB
            .setTransaction(daoTransaction.getTransactionByKeyword(transactionInstanceBusiness.getTransactionKeyword()));

      entityManager.persist(transactionInstanceDB);

   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isDomainExisting(String domainKeyword) throws RecordingException {
      return daoDomain.isDomainExisting(domainKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isTransactionExisting(String transactionKeyword) throws RecordingException {
      return daoTransaction.isTransactionExisting(transactionKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isActorExisting(String actorKeyword) throws RecordingException {
      return daoActor.isActorExisting(actorKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createDomain(String domainKeyword) throws RecordingException {
      daoDomain.createDomain(domainKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createTransaction(String transactionKeyword) throws RecordingException {
      daoTransaction.createTransaction(transactionKeyword);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void createActor(String actorKeyword) throws RecordingException {
      daoActor.createActor(actorKeyword);
   }

   /**
    * Get the number of transaction-instances registered in the persistence space
    *
    * @return the number of transaction-instances registered
    */
   public Long getNumberOfRegisteredTransactionInstances() {
      return (Long) entityManager
            .createQuery("select count(ti) from TransactionInstanceDB ti")
            .getSingleResult();
   }
}
