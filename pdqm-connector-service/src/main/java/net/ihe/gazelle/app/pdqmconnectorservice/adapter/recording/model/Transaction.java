package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;



/**
 * <p>Transaction class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "tf_transaction", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "keyword"}))
@SequenceGenerator(name = "tf_transaction_sequence", sequenceName = "tf_transaction_id_seq", allocationSize=1)
@XmlAccessorType(XmlAccessType.FIELD)

public class Transaction extends TFObject implements java.io.Serializable {

    /** Serial ID version of this object */
    private static final long serialVersionUID = -5994534457658398088L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_transaction_sequence")
    @XmlTransient
    private Integer id;

    //	Constructors
    /**
     * <p>Constructor for Transaction.</p>
     */
    public Transaction() {
        //Empty constructor
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId()
    {
        return this.id;
    }

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
