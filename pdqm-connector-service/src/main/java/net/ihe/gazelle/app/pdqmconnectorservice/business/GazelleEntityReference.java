package net.ihe.gazelle.app.pdqmconnectorservice.business;

/**
 * Carry information of any Gazelle business entity to be referred to.
 *
 * @author ceoche
 */
public class GazelleEntityReference {

   private String keyword;
   private String name;

   /**
    * Constructor of an entity reference
    *
    * @param keyword Keyword of the business entity
    * @param name    Name of the business entity, for information only
    */
   public GazelleEntityReference(String keyword, String name) {
      this.keyword = keyword;
      this.name = name;
   }

   /**
    * Get the keyword of the referred business entity
    *
    * @return the entity keyword
    */
   public String getKeyword() {
      return keyword;
   }

   /**
    * Get the name of the referred business entity (for information only)
    *
    * @return the entity name
    */
   public String getName() {
      return name;
   }

}
