package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.preferences.Preferences;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import net.ihe.gazelle.sb.pdqm.application.PDQmParameters;
import org.hl7.fhir.r4.model.OperationOutcome;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Simulation Gate implementation for PDQm Connector.
 */
@Default
public class SimulationGateImpl implements SimulationGate<FhirSearchParameters, FhirResources> {

   private OperationalPreferencesService operationalPreferencesService;
   private PatientSearchClient client;

   /**
    * Default constructor, used for injection.
    *
    * @param operationalPreferencesService {@link OperationalPreferencesService} used to retrieve Patient Registry's URL.
    */
   @Inject
   public SimulationGateImpl(OperationalPreferencesService operationalPreferencesService) {
      this.operationalPreferencesService = operationalPreferencesService;
   }

   /**
    * Package-private constructor used for test purposes.
    *
    * @param url {@link URL} to be used by this to instantiate the processing service to retrieve patients.
    */
   @Package
   SimulationGateImpl(URL url) {
      this.client = new PatientSearchClient(url);
   }

   /**
    * Package-private constructor used for test purposes.
    *
    * @param service {@link ProcessingService} to be used by this to retrieve Patients.
    */
   @Package
   SimulationGateImpl(ProcessingService service) {
      this.client = new PatientSearchClient(service);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public FhirResources process(FhirSearchParameters content) throws SearchException {
      if (client == null) {
         initializeSearchClient();
      }

      FhirResources resources = new FhirResources();
      SearchCriteria searchCriteria = new SearchCriteria();
      content.getSearchParameters().forEach(searchParam -> {
         if (PDQmParameters.ID.isEqualToPdqmParameter(searchParam.getSearchParameter())) {

            SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
            searchCriterion.setValue(searchParam.getValue());
            searchCriteria.addSearchCriterion(searchCriterion);
         }
      });


      List<Patient> patients = client.search(searchCriteria);
      for (Patient patient : patients) {
         try {
            resources.addResource(BusinessToFhirConverter.patientToFhirResource(patient));
         } catch (FhirConvertionException e) {
            Exception searchException = new SearchException(String.format("Unable to transform Patient with UUID [%s] as FHIR Resource.",
                    patient.getUuid()), e);
            OperationOutcome.OperationOutcomeIssueComponent issue = new OperationOutcome.OperationOutcomeIssueComponent();
            issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
            issue.setCode(OperationOutcome.IssueType.EXCEPTION);
            issue.setDiagnostics(searchException.getMessage());
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.getIssue().add(issue);
            resources.addResource(operationOutcome);
         }
      }

      return resources;
   }

   /**
    * Initialize the Search Client using the Operational Preferences Service.
    *
    * @throws SearchException if the service cannot be instantiated.
    */
   private void initializeSearchClient() throws SearchException {
      String patientRegistryUrl = null;
      try {
         patientRegistryUrl = this.operationalPreferencesService.
               getStringValue(Preferences.PATIENT_REGISTRY_URL.getNamespace().getValue(), Preferences.PATIENT_REGISTRY_URL.getKey());
         this.client = new PatientSearchClient(new URL(patientRegistryUrl));
      } catch (NamespaceException | PreferenceException e) {
         throw new SearchException(String.format("Unable to retrieve [%s] Preference !", Preferences.PATIENT_REGISTRY_URL.getKey()));
      } catch (MalformedURLException e) {
         throw new SearchException(String.format("Preference [%s] with value [%s] is not a valid URL !", Preferences.PATIENT_REGISTRY_URL.getKey(),
               patientRegistryUrl));
      } catch (WebServiceException e) {
         throw new SearchException("Can't connect to patient registry !");
      }
   }
}
