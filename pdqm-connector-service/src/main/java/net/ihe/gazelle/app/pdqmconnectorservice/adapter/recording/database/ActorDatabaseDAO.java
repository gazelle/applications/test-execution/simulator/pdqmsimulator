package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.dependencyinjection.EntityManagerProducer;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Actor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Database access to persisted Actors
 */
public class ActorDatabaseDAO {

   private final EntityManager entityManager;

   /**
    * Constructor
    *
    * @param entityManager an operational EntityManager to access JPA entities.
    */
   @Inject
   public ActorDatabaseDAO(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   /**
    * Verify if the given actor already exist in the storage space
    *
    * @param actorKeyword keyword of the actor to verify
    *
    * @return true if it is already registered in the storage space, false otherwise.
    */
   public boolean isActorExisting(String actorKeyword) {
      return (Boolean) entityManager
            .createNativeQuery("SELECT CASE WHEN EXISTS (" +
                  "    SELECT id" +
                  "    FROM tf_actor" +
                  "    where keyword = ?" +
                  ") " +
                  "THEN CAST(1 AS BIT) " +
                  "ELSE CAST(0 AS BIT) END")
            .setParameter(1, actorKeyword)
            .getSingleResult();
   }

   /**
    * load an Actor from the storage space by its keyword
    *
    * @param keyword keyword of the actor to load.
    *
    * @return the actor with the given keyword or null if not found
    */
   public Actor getActorByKeyword(String keyword) {
      try {
         return entityManager
               .createQuery("SELECT a from Actor a where keyword = :keyword", Actor.class)
               .setParameter("keyword", keyword)
               .getSingleResult();
      } catch (NoResultException e) {
         return null;
      }
   }

   /**
    * Create an actor in the persistence storage with the specified keyword.
    *
    * @param keyword keyword of the
    */
   public void createActor(String keyword) {
      Actor actor = new Actor();
      actor.setKeyword(keyword);
      actor.setName(keyword);
      actor.setCanActAsResponder(true);
      entityManager.persist(actor);
   }

   /**
    * Get the number of actor registered in the persistence storage
    *
    * @return the number of actor registered
    */
   public Long getNumberOfRegisteredActors() {
      return (Long) entityManager
            .createQuery("select count(a) from Actor a")
            .getSingleResult();
   }
}
