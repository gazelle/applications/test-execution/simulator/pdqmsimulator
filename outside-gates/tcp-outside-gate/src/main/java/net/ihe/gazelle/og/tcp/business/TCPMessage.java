package net.ihe.gazelle.og.tcp.business;

import net.ihe.gazelle.modelapi.messaging.business.TransportMessageInstance;

import java.util.Arrays;

public class TCPMessage extends TransportMessageInstance {

    byte[] payload;

    /**
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param payload         payload of the transport message
     */
    public TCPMessage(String uuid, String standardKeyword, byte[] payload) {
        super(uuid, standardKeyword);
        this.payload = payload.clone();
    }

    /**
     * Get payload
     * @return payload of the transport message
     */
    public byte[] getPayload() {
        return payload;
    }

    /**
     * Check if two entity have the same identity.
     *
     * @param o object to check
     * @return true if both entity have the same identity, false otherwise.
     */
    public boolean identityEqual(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof TCPMessage)) {
            return false;
        } else {
            TCPMessage that = (TCPMessage) o;
            return this.getUuid().equals(that.getUuid());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TCPMessage)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        TCPMessage that = (TCPMessage) o;

        return Arrays.equals(payload, that.payload);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(payload);
        return result;
    }
}
