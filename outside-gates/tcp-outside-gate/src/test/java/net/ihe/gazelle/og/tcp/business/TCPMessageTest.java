package net.ihe.gazelle.og.tcp.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link TCPMessage}
 */
class TCPMessageTest {

    private static final String UUID = "UUID";
    private static final String STANDARD_KEYWORD = "standardKeyword";
    private static final byte[] PAYLOAD = "payload".getBytes(StandardCharsets.UTF_8);

    /**
     * Test for payload property getter.
     */
    @Test
    @Covers(requirements = "TCPOG-1")
    void getPayload() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertArrayEquals(PAYLOAD, tcpMessage.getPayload(), "Getter shall return the value of the payload !");
    }

    /**
     * Test identityEqual with two entity with same identity.
     */
    @Test
    void identityEqualTrue() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);
        TCPMessage tcpMessage1 = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertTrue(tcpMessage.identityEqual(tcpMessage1), "Two TCPMessage with same UUID shall be identityEqual.");
    }

    /**
     * Test identityEqual with same object
     */
    @Test
    void identityEqualSameObject() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertTrue(tcpMessage.identityEqual(tcpMessage), "TCPMessage shall be identityEqual to himself.");
    }

    /**
     * Test identityEqual with instance of another class
     */
    @Test
    void identityEqualOtherClass() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertFalse(tcpMessage.identityEqual(13), "TCPMessage shall not be identityEqual to an instance of another class.");
    }

    /**
     * Test identityEqual with two entity with same identity but different value.
     */
    @Test
    void identityEqualDifferentValue() {
        TCPMessage tcpMessage = new TCPMessage(UUID, "Tarte aux pommes", PAYLOAD);
        TCPMessage tcpMessage1 = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertTrue(tcpMessage.identityEqual(tcpMessage1), "Two TCPMessage with same UUID shall be identityEqual.");
    }

    /**
     * Test identityEqual with two entity with different uuid.
     */
    @Test
    void identityEqualDifferentUUID() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);
        TCPMessage tcpMessage1 = new TCPMessage("Tarte aux pommes", STANDARD_KEYWORD, PAYLOAD);

        assertFalse(tcpMessage.identityEqual(tcpMessage1), "Two TCPMessage with different UUID shall not be identityEqual.");
    }

    /**
     * Test object is equal to itself.
     */
    @Test
    void equalsSameObject() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertTrue(tcpMessage.equals(tcpMessage), "Object shall be equal to itself.");
    }

    /**
     * Test object is not equal with an instance of another class
     */
    @Test
    void equalsOtherClass() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertFalse(tcpMessage.equals(13), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test object is not equal with null
     */
    @Test
    void equalsNull() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertFalse(tcpMessage.equals(null), "Object shall not be equal to null.");
    }

    /**
     * Test object is not equal if super is not equal.
     */
    @Test
    void equalsSuper() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);
        TCPMessage tcpMessage1 = new TCPMessage(UUID, "Tarte aux pommes", PAYLOAD);

        assertFalse(tcpMessage.equals(tcpMessage1), "Object shall not be equal if super is not equal.");
    }

    /**
     * Test objects are equal if they have the same value
     */
    @Test
    void equalsSameValue() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);
        TCPMessage tcpMessage1 = new TCPMessage("Tarte aux pommes", STANDARD_KEYWORD, PAYLOAD);

        assertTrue(tcpMessage.equals(tcpMessage1), "Object shall be equal if they have the same value !");
    }

    /**
     * Test hashCode.
     */
    @Test
    void testHashCode() {
        TCPMessage tcpMessage = new TCPMessage(UUID, STANDARD_KEYWORD, PAYLOAD);

        assertNotNull(tcpMessage.hashCode(), "Hash Code shall not be null.");
    }
}