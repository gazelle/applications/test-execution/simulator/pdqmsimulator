package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.modelapi.messaging.business.TransportMessageInstance;
import net.ihe.gazelle.og.http.business.AbstractHTTPMessage;

/**
 * abstract class to define a commmon base for all Fhir messages
 */
@Package
@Covers(requirements = "FHIROG-2")
abstract class AbstractFhirMessage<T extends AbstractHTTPMessage> extends TransportMessageInstance {

    FhirVersion fhirVersion;
    T httpMessage;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion     version of Fhir
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    @Package
    AbstractFhirMessage(String uuid, String standardKeyword, FhirVersion fhirVersion) {
        super(uuid, standardKeyword);
        setFhirVersion(fhirVersion);
    }

    /**
     * Getter for the fhirVersion property.
     *
     * @return the value of the property.
     */
    public FhirVersion getFhirVersion() {
        return fhirVersion;
    }

    /**
     * Setter for the fhirVersion property.
     *
     * @param fhirVersion value to set to the property.
     * @throws IllegalArgumentException if the format is null
     */
    public void setFhirVersion(FhirVersion fhirVersion) {
        if (fhirVersion != null) {
            this.fhirVersion = fhirVersion;
        } else {
            throw new IllegalArgumentException("Format shall not be null !");
        }
    }

    /**
     * Getter for the httpMessage property.
     *
     * @return the value of the property.
     */
    public T getHttpMessage() {
        return httpMessage;
    }

    /**
     * Setter for the httpMessage property.
     *
     * @param httpMessage value to set to the property.
     */
    public void setHttpMessage(T httpMessage) {
        this.httpMessage = httpMessage;
    }

    /**
     * Check if two entity have the same identity.
     *
     * @param o object to check
     * @return true if both entity have the same identity, false otherwise.
     */
    public boolean identityEqual(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof AbstractFhirMessage)) {
            return false;
        } else {
            AbstractFhirMessage that = (AbstractFhirMessage) o;
            return this.getUuid().equals(that.getUuid());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractFhirMessage)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        AbstractFhirMessage<?> that = (AbstractFhirMessage<?>) o;

        if (fhirVersion != that.fhirVersion) {
            return false;
        }
        return httpMessage != null ? httpMessage.equals(that.httpMessage) : that.httpMessage == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (fhirVersion != null ? fhirVersion.hashCode() : 0);
        result = 31 * result + (httpMessage != null ? httpMessage.hashCode() : 0);
        return result;
    }
}
