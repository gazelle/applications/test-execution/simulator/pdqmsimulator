package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.hl7.fhir.r4.model.SearchParameter;

/**
 * Search parameter concretion
 */
@Covers(requirements = "FHIROG-24")
public class SearchParamImpl extends SearchParameter{


    private String value;

    /**
     * constructor for SearchParamImpl
     * @param value the value of the parameter
     */
    public SearchParamImpl(String value) {
        this.setValue(value);
    }


    /**
     * get the search parameter value
     *
     * @return the search parameter value
     */
    public String getValue() {
        return value;
    }

    /**
     * set the search parameter value
     *
     * @param value the search parameter value to set
     */
    public void setValue(String value) {
        if (value == null) {
            throw new IllegalArgumentException("value shall not be null");
        }
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchParamImpl)) return false;
        SearchParamImpl that = (SearchParamImpl) o;
        return value.equals(that.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
