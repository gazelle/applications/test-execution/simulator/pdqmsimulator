package net.ihe.gazelle.og.fhir.business;

public enum FhirVersion {
    R4,
    R5
}
