package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;

import java.util.ArrayList;
import java.util.List;

/**
 * Fhir Search Request Message
 */
@Covers(requirements = "FHIROG-22")
public class FhirSearchRequest extends FhirRequest {

    private List<SearchParamImpl> searchParameters = new ArrayList<>();

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion     version of Fhir
     * @param generalParam    the request general parameter
     * @param base            the request base url
     * @param mimeType        the request mime type
     * @param id              the requested resource id
     * @param vid             the requested resource version id
     * @param compartment     the requested server compartment
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public FhirSearchRequest(String uuid, String standardKeyword, FhirVersion fhirVersion, GeneralParam generalParam, String base, String mimeType,
                             String id, String vid, String compartment, String type) {
        super(uuid, standardKeyword, fhirVersion, generalParam, base, mimeType, id, vid, compartment, type);
    }

    /**
     * Get list of search parameter
     *
     * @return list of search parameter
     */
    public List<SearchParamImpl> getSearchParameters() {
        return new ArrayList<>(searchParameters);
    }

    /**
     * Add a search parameter to list of search parameter
     *
     * @param searchParameter the search parameter to add
     */
    public void addSearchParameter(SearchParamImpl searchParameter) {
        this.searchParameters.add(searchParameter);
    }

    /**
     * Remove a search parameter to list of search parameter
     *
     * @param searchParameter the search parameter to remove
     */
    public void removeSearchParameter(SearchParamImpl searchParameter) {
        this.searchParameters.remove(searchParameter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean identityEqual(Object o) {
        if (!(o instanceof FhirSearchRequest)) {
            return false;
        } else {
            return super.identityEqual(o);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        FhirSearchRequest that = (FhirSearchRequest) o;

        return searchParameters != null ? searchParameters.equals(that.searchParameters) : that.searchParameters == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (searchParameters != null ? searchParameters.hashCode() : 0);
        return result;
    }
}
