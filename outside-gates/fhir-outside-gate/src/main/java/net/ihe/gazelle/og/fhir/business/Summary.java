package net.ihe.gazelle.og.fhir.business;

public enum Summary {
    TRUE,
    TEXT,
    DATA,
    COUNT,
    FALSE
}
