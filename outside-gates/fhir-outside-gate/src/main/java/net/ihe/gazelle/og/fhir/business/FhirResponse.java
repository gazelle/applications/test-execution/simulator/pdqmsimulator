package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.http.business.HTTPResponse;
import org.hl7.fhir.r4.model.Resource;

/**
 * Fhir Response message
 */
@Covers(requirements = "FHIROG-5")
public class FhirResponse extends AbstractFhirMessage<HTTPResponse> {

    private Resource resource;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid        value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion version of Fhir
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public FhirResponse(String uuid, String standardKeyword, FhirVersion fhirVersion) {
        super(uuid, standardKeyword, fhirVersion);
    }

    /**
     * Get resource
     * @return the fhir resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Set Resource
     * @param resource to return
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean identityEqual(Object o) {
        if (!(o instanceof FhirResponse)) {
            return false;
        } else {
            return super.identityEqual(o);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FhirResponse)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        FhirResponse that = (FhirResponse) o;

        return resource != null ? resource.equals(that.resource) : that.resource == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (resource != null ? resource.hashCode() : 0);
        return result;
    }
}
