package net.ihe.gazelle.og.fhir.business;

import java.util.ArrayList;
import java.util.List;

public class GeneralParam {

    private Format format;
    private Summary summary;
    private boolean pretty;
    private List<String> elements = new ArrayList<>();

    /**
     * Constructor with all variables
     *
     * @param format  format of the response
     * @param summary Ask for a predefined short form of the resource in response
     * @param pretty  Ask for a pretty printed response for human convenience
     */
    public GeneralParam(Format format, Summary summary, boolean pretty) {
        this.format = format;
        this.summary = summary;
        this.pretty = pretty;
    }

    /**
     * Get format
     *
     * @return format of the response
     */
    public Format getFormat() {
        return format;
    }

    /**
     * set format
     *
     * @param format
     */
    public void setFormat(Format format) {
        this.format = format;
    }

    /**
     * get summary
     *
     * @return summary
     */
    public Summary getSummary() {
        return summary;
    }

    /**
     * Set summary
     *
     * @param summary
     */
    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    /**
     * get pretty
     *
     * @return
     */
    public boolean isPretty() {
        return pretty;
    }

    /**
     * set pretty
     *
     * @param pretty
     */
    public void setPretty(boolean pretty) {
        this.pretty = pretty;
    }

    /**
     * get list of element
     *
     * @return list of string
     */
    public List<String> getElements() {
        return elements;
    }

    /**
     * Add an element to the list of element
     *
     * @param element element to add
     */
    public void addElement(String element) {
        this.elements.add(element);
    }

    /**
     * Remove an element to the list of element
     *
     * @param element element to remove
     */
    public void removeElement(String element) {
        this.elements.remove(element);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeneralParam that = (GeneralParam) o;

        if (pretty != that.pretty) {
            return false;
        }
        if (format != that.format) {
            return false;
        }
        if (summary != that.summary) {
            return false;
        }
        return elements != null ? elements.equals(that.elements) : that.elements == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = format != null ? format.hashCode() : 0;
        result = 31 * result + (summary != null ? summary.hashCode() : 0);
        result = 31 * result + (pretty ? 1 : 0);
        result = 31 * result + (elements != null ? elements.hashCode() : 0);
        return result;
    }
}
