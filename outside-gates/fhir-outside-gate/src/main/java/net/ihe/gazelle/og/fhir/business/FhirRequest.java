package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.og.http.business.HTTPRequest;

import java.util.Objects;

/**
 * abstract class to define common base for all Fhir Requests
 */
@Package
@Covers(requirements = "FHIROG-8")
abstract class FhirRequest extends AbstractFhirMessage<HTTPRequest> {

    private GeneralParam generalParam;
    private String base;
    private String mimeType;
    private String id;
    private String vid;
    private String compartment;
    private String type;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion     version of Fhir
     * @param generalParam    the request general parameter
     * @param base            the request base url
     * @param mimeType        the request mime type
     * @param id              the requested resource id
     * @param vid             the requested resource version id
     * @param compartment     the requested server compartment
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public FhirRequest(String uuid, String standardKeyword, FhirVersion fhirVersion, GeneralParam generalParam, String base, String mimeType,
                       String id, String vid, String compartment, String type) {
        super(uuid, standardKeyword, fhirVersion);
        this.setGeneralParam(generalParam);
        this.setBase(base);
        this.setMimeType(mimeType);
        this.setId(id);
        this.setVid(vid);
        this.setCompartment(compartment);
        this.setType(type);
    }

    /**
     * get fhir general parameters
     *
     * @return request general parameters
     */
    public GeneralParam getGeneralParam() {
        return generalParam;
    }

    /**
     * set fhir general parameters
     *
     * @param generalParam the request general parameters
     */
    public void setGeneralParam(GeneralParam generalParam) {
        this.generalParam = generalParam;
    }

    /**
     * get base url
     *
     * @return base url
     */
    public String getBase() {
        return base;
    }

    /**
     * set base url
     *
     * @param base url
     */
    public void setBase(String base) {
        if (base == null) {
            throw new IllegalArgumentException("url base shall not be null");
        }
        this.base = base;
    }

    /**
     * get mime type
     *
     * @return mime type
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * set mime type
     *
     * @param mimeType the mime type
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * get request resource id
     *
     * @return resource id
     */
    public String getId() {
        return id;
    }

    /**
     * set request resource id
     *
     * @param id resource id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * get request resource version id
     *
     * @return resource version id
     */
    public String getVid() {
        return vid;
    }

    /**
     * set request resource version id
     *
     * @param vid resource version id
     */
    public void setVid(String vid) {
        this.vid = vid;
    }

    /**
     * get server compartment
     *
     * @return server compartment
     */
    public String getCompartment() {
        return compartment;
    }

    /**
     * set server compartment
     *
     * @param compartment, the server compartment
     */
    public void setCompartment(String compartment) {
        this.compartment = compartment;
    }

    /**
     * get resource type
     *
     * @return resource type
     */
    public String getType() {
        return type;
    }

    /**
     * set resource type
     *
     * @param type, the resource type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FhirRequest)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        FhirRequest that = (FhirRequest) o;
        return Objects.equals(generalParam, that.generalParam) &&
                base.equals(that.base) &&
                Objects.equals(mimeType, that.mimeType) &&
                Objects.equals(id, that.id) &&
                Objects.equals(vid, that.vid) &&
                Objects.equals(compartment, that.compartment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (generalParam != null ? generalParam.hashCode() : 0);
        result = 31 * result + (base != null ? base.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (vid != null ? vid.hashCode() : 0);
        result = 31 * result + (compartment != null ? compartment.hashCode() : 0);
        return result;
    }
}
