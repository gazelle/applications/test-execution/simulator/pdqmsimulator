package net.ihe.gazelle.og.http.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.tcp.business.TCPMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HTTPRequestTest {


    /**
     * tested class
     */
    private HTTPRequest httpRequest;
    private HTTPRequest httpRequest2;

    @Covers(requirements = {"HTTPOG-6"})
    @BeforeEach
    public void init() {
        String uuid = "7d16f712-bcff-11ea-b3de-0242ac130004";
        String standardKeyword = "HTTP";
        HTTPMethod httpMethod = HTTPMethod.GET;
        byte[] httpBody = new byte[] {1,2,3};
        TCPMessage rawTCPMessage = new TCPMessage(uuid,standardKeyword,new byte[]{1});
        String httpRequestTarget = "target";
        HTTPVersion httpVersion = HTTPVersion.V1_1;
        httpRequest = new HTTPRequest(uuid, standardKeyword, httpBody, rawTCPMessage, httpMethod, httpRequestTarget, httpVersion);
        httpRequest2 = new HTTPRequest(uuid, standardKeyword, httpBody, rawTCPMessage, httpMethod, httpRequestTarget, httpVersion);
    }

    @Covers(requirements = {"HTTPOG-8"})
    @Test
    public void httpRequestTargetTest() {
        String expectedHttpRequestTarget = "expectedTarget";
        httpRequest.setHttpRequestTarget(expectedHttpRequestTarget);
        assertEquals(expectedHttpRequestTarget,httpRequest.getHttpRequestTarget(), "The expectedHttpRequestTarget must equal to " + expectedHttpRequestTarget);
    }

    @Covers(requirements = {"HTTPOG-7"})
    @Test
    public void httpMethodTest() {
        HTTPMethod expectedHttpMethod = HTTPMethod.POST;
        httpRequest.setHttpMethod(expectedHttpMethod);
        assertEquals(expectedHttpMethod,httpRequest.getHttpMethod(), "The expectedHttpMethod must equal to " + expectedHttpMethod);
    }

    @Test
    public void equalsTest() {
        assertEquals(httpRequest, httpRequest2,"identical objects shall be equal");
        assertEquals(httpRequest.hashCode(), httpRequest.hashCode(), "hashcode shall be equal too");
    }

    @Test
    public void paramParseTest() {
        String param1 = "param";
        String value1 = "1";
        String param2 = "param1";
        String value2 = "3";

        httpRequest.setHttpRequestTarget("http://host.fr/path/123?"+param1+"="+value1+"&"+param2+"="+value2);
        assertTrue(httpRequest.getParameters().containsKey(param1));
        assertEquals(1,httpRequest.getParameters().get(param1).size());
        assertEquals(value1, httpRequest.getParameters().get(param1).get(0), "param value should be registered");

    }

    @Test
    public void paramParseMultipleTest() {
        String param1 = "param";
        String value1 = "1";
        String param2 = "param1";
        String value2 = "3";

        httpRequest.setHttpRequestTarget("http://host.fr/path/123?"+param1+"="+value1+"&"+param2+"="+value2+"&"+param1+"="+value2);

        assertEquals(2,httpRequest.getParameters().get(param1).size());
        assertEquals(value1, httpRequest.getParameters().get(param1).get(0), "param value should be registered");
        assertEquals(value2, httpRequest.getParameters().get(param1).get(1), "param value should be registered");

    }

    @Test
    public void paramParseIncorrectUrlTest() {
        String param1 = "param";
        String value1 = "1";
        String param2 = "param1";
        String value2 = "3";

        httpRequest.setHttpRequestTarget("http://host.fr/path/123?"+param1+"="+value1+"&"+param2+"="+value2+param1+"="+value2);

        assertThrows(IllegalArgumentException.class,()->httpRequest.getParameters(),"broken url shall return error");

    }
}
