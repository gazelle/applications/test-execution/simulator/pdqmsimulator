package net.ihe.gazelle.og.http.business;

public enum HTTPVersion {
    V1_1,
    V2_0
}
