package net.ihe.gazelle.og.http.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.modelapi.messaging.business.MessageMetadataInstance;
import net.ihe.gazelle.modelapi.messaging.business.TransportMessageInstance;
import net.ihe.gazelle.og.tcp.business.TCPMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AbstractHTTPMessage extends TransportMessageInstance {

    private byte[] httpBody;
    private HTTPVersion httpVersion;
    private List<MessageMetadataInstance> httpField = new ArrayList<>();
    private TCPMessage rawTCPMessage;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid     value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param httpBody body of the http message
     * @param rawTCPMessage TCP message
     * @param httpVersion HTTP version
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public AbstractHTTPMessage(String uuid, String standardKeyword, byte[] httpBody, TCPMessage rawTCPMessage, HTTPVersion httpVersion) {
        super(uuid, standardKeyword);
        this.setHttpBody(httpBody);
        this.setRawTCPMessage(rawTCPMessage);
        this.setHttpVersion(httpVersion);
    }

    public byte[] getHttpBody() {
        return httpBody;
    }

    @Covers(requirements = {"HTTPOG-4"})
    public void setHttpBody(byte[] httpBody) {
        if (httpBody == null) {
            throw new IllegalArgumentException("http body shall not be null");
        }
        this.httpBody = httpBody;
    }

    public HTTPVersion getHttpVersion() {
        return httpVersion;
    }

    @Covers(requirements = {"HTTPOG-3"})
    public void setHttpVersion(HTTPVersion httpVersion) {
        if (httpVersion == null) {
            throw new IllegalArgumentException("http version shall not be null");
        }
        this.httpVersion = httpVersion;
    }

    public List<MessageMetadataInstance> getHttpHeader() {
        return httpField;
    }

    @Covers(requirements = {"HTTPOG-5"})
    public void setHttpHeader(List<MessageMetadataInstance> httpHeader) {
        if (httpHeader == null) {
            throw new IllegalArgumentException("http header shall not be null");
        }
        this.httpField = httpHeader;
    }

    public void addHttpHeader(MessageMetadataInstance httpHeader) {
        httpField.add(httpHeader);
    }

    public void removeHttpHeader(MessageMetadataInstance httpHeader) {
        httpField.remove(httpHeader);
    }

    public TCPMessage getRawTCPMessage() {
        return rawTCPMessage;
    }

    @Covers(requirements = {"HTTPOG-1","HTTPOG-2"})
    public void setRawTCPMessage(TCPMessage rawTCPMessage) {
        if (rawTCPMessage == null) {
            throw new IllegalArgumentException("rawTCPMessage shall not be null");
        }
        this.rawTCPMessage = rawTCPMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractHTTPMessage)) return false;
        if (!super.equals(o)) return false;
        AbstractHTTPMessage that = (AbstractHTTPMessage) o;
        return Arrays.equals(httpBody, that.httpBody) &&
                httpVersion == that.httpVersion &&
                httpField.equals(that.httpField) &&
                rawTCPMessage.equals(that.rawTCPMessage);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(httpBody);
        result = 31 * result + httpVersion.hashCode();
        result = 31 * result + httpField.hashCode();
        result = 31 * result + rawTCPMessage.hashCode();
        return result;
    }
}
