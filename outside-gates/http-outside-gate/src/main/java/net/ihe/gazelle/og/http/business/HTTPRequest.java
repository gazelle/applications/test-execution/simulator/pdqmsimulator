package net.ihe.gazelle.og.http.business;


import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.tcp.business.TCPMessage;

import java.util.*;

@Covers(requirements = {"HTTPOG-6"})
public class HTTPRequest extends AbstractHTTPMessage {

    private String httpRequestTarget;
    private HTTPMethod httpMethod;
    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid       value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param httpBody   body of the http message
     * @param rawTCPMessage TCP message
     * @param httpMethod HTTP method
     * @param httpRequestTarget Target of the HTTP Request
     * @param httpVersion HTTP Version
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public HTTPRequest(String uuid, String standardKeyword, byte[] httpBody, TCPMessage rawTCPMessage, HTTPMethod httpMethod, String httpRequestTarget, HTTPVersion httpVersion) {
        super(uuid, standardKeyword, httpBody, rawTCPMessage, httpVersion);
        this.setHttpMethod(httpMethod);
        this.setHttpRequestTarget(httpRequestTarget);
    }

    public String getHttpRequestTarget() {
        return httpRequestTarget;
    }

    @Covers(requirements = {"HTTPOG-8"})
    public void setHttpRequestTarget(String httpRequestTarget) {
        if (httpRequestTarget == null) {
            throw new IllegalArgumentException("http request target shall not be null");
        }
        this.httpRequestTarget = httpRequestTarget;
    }

    public HTTPMethod getHttpMethod() {
        return httpMethod;
    }

    @Covers(requirements = {"HTTPOG-7"})
    public void setHttpMethod(HTTPMethod httpMethod) {
        if (httpMethod == null) {
            throw new IllegalArgumentException("http method target shall not be null");
        }
        this.httpMethod = httpMethod;
    }

    public Map<String, List<String>> getParameters() {
        Map<String, List<String>> map = new HashMap<>();
        if(httpRequestTarget.contains("?")) {
            String query = httpRequestTarget.split("\\?")[1];


            List<String> paramPairs = Arrays.asList(query.split("&"));
            paramPairs.forEach(pair -> {
                String[] pairParts = pair.split("=");
                if (2 != pairParts.length) {
                    throw new IllegalArgumentException("Parse failed: " + httpRequestTarget + ", param shall have a key and value");
                }
                String key = pairParts[0];
                String value = pairParts[1];
                List<String> paramValues = map.containsKey(key) ? map.get(key) : new ArrayList<>();
                paramValues.add(value);
                map.put(key, paramValues);
            });
        }
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HTTPRequest)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        HTTPRequest that = (HTTPRequest) o;

        if (!Objects.equals(httpRequestTarget, that.httpRequestTarget)) {
            return false;
        }
        return httpMethod == that.httpMethod;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (httpRequestTarget != null ? httpRequestTarget.hashCode() : 0);
        result = 31 * result + (httpMethod != null ? httpMethod.hashCode() : 0);
        return result;
    }
}
